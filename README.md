EXPECT: Efficient eXtended Precision aCcumulaTors
=================================================

TODO: high level explanation of goals and how it works

Files organisation:

- `src/inc/`: templated classes
- `src/tests/`: C++ models tests
- `src/ip/`: IPs for Vivado HLS
- `src/compat/`: compatibility headers to use code intended for Vivado HLS with a regular compiler. This allows to use IPs code without an installation of Vivado HLS' simulation headers
- `default.nix`: Nix "entry points". The main logic happens here, it could be easily extended for new IPs/tests/etc
- `nix/`: other Nix files

Note about Nix: the code can be used without Nix, with any other package managers and Vivado HLS graphical interface.
Nix is used for continuous integration anyway: don't install it if you don't want to, push your commits and let the magic happen.

Extended precision floating point multiplication-accumulation
-------------------------------------------------------------

The main class, `expect`, has four template parameters:
- B: number of accumulator's lines
- W: width of each line
- R: redundancy rate
- Zero: position of the "zero"

The accumulator needs B×W bits of memory but is equivalent to a fixed-point of B×R bits.
The Zero parameter is the position of the bit of weight 0 in this equivalent fixed-point.

The implementation is split in two parts: low-level interface (direct manipulating of the accumulator's internal state) and high-level interface (arithmetic operators).

### Operators

The table below lists all `expect`'s operators.
The categories stand for HL=high-level, LL=low-level, and D=debug (operators that should not be used in hardware code).
For the sake of simplicity, integer arguments are designed with `int`, while they are in fact custom-sized integers.
`float` arguments can be any floating point format wrapper.

All functions taking/returning floating point values can be instantiated with any floating point format (given as a template parameter), no matters what are the class parameters.
The functions are extensively documented through comments.

Important notice: the current code needs some modifications to achieve an initiation interval of 1 (need to skew some loops and probably declare some false dependency).

| name                   | arguments            | cat. | explanation                                                                                           | implemented |
|------------------------|----------------------|------|-------------------------------------------------------------------------------------------------------|-------------|
| `accumulate`           | float                | HL   | accumulate a floating point value in the accumulator                                                  | ✓           |
| `mac`                  | float, float         | HL   | accumulate a product of two floating point values in the accumulator                                  | ✓           |
| `propagate`            |                      | HL   | propagate carries and return the value as a floating point value                                      | ✓           |
| `accumulate`           | expect               | HL   | accumulate an accumulator in the accumulator                                                          | ✓           |
| `mac`                  | expect, expect       | HL   | accumulate the product of two accumulators in the accumulator                                         | ✗           |
| `mac`                  | expect, float        | HL   | accumulate the product of one accumulator and one floating point value in the accumulator             | ✗           |
| `operator ==`          | expect               | HL   | test if two expect accumulators have the same value                                                   | ✓           |
| `get_line`             | int                  | LL   | get a line                                                                                            | ✓           |
| `set_line`             | int, int             | LL   | set a line                                                                                            | ✓           |
| `get_line_overlap`     | int                  | LL   | get a line's overlap bits (W-R MSB)                                                                   | ✓           |
| `get_line_significant` | int                  | LL   | get a line's significant bits (R LSB)                                                                 | ✓           |
| `accumulate`           | int, int             | LL   | accumulate a word in a line                                                                           | ✓           |
| `accumulate`           | int, int             | LL   | accumulate a word at a bit position (aligned on accumulator's LSB), with shifting                     | ✓           |
| `accumulate`           | int, int             | LL   | accumulate a word at a weight position (aligned on Zero)                                              | ✓           |
| `get_bit`              | int                  | LL/D | get a bit (argument is signed, aligned on zero; precondition: `propagate()`)                          | ✓           |
| `reset`                |                      | D    | set the accumulator's value to zero (it would probably need some changes to be efficient on hardware) | ✓           |
| `operator <<`          | std::ostream, expect | D    | write the accumulator's details to an output stream                                                   | ✓           |

IPs
---

These IPs are thin wrappers around `expect`'s operators. They only implement the operators themselves, the memory is considered outside of the IP.
- `accumulate-float`
- `compare`
- `mac-float-float`
- `propagate`

This IPs are more advanced IPs with several operations/loops and/or they include the memory:
- `mmult-expect`: this is a WIP version
- TODO: floating point matrix product
- TODO: memory footprint (expect not as an argument)

Note on Xilinx tools versions: some Vivado version (like 2019.1) use very old clang version (3.1) which has bug and raise errors like:
```
./expect.h:260:27: error: invalid operands to binary expression ('word_t' (aka 'intw<60, true>') and 'word_t')
...
          (get_line(line) + e.get_line(line))
           ~~~~~~~~~~~~~~ ^ ~~~~~~~~~~~~~~~~
hw.cpp:4:5: note: in instantiation of function template specialization 'expect<565, 60, 1, 298>::accumulate<565, 60, 298>' requested here
  l.accumulate(r);
   ^
[...]
./intwrapper.h:370:37: note: candidate template ignored: substitution failure [with W2 = 60]
    intw<addwidth<W2>(), is_signed> operator +(const intw<W2, is_signed>& rhs) const {
                                    ^
```
Therefore, IPs (or some IPs) must be synthesized with a newer version. They had been tested with Vitis 2020.2


Integer and floating point wrappers
-----------------------------------

Classes `floatw` and `intw` are two wrapper classes around integer and floating point formats. The former uses the latter.
They allow to write legible code using such formats with type safety.
For example, `f.explicit_mantissa()` evaluates to a floating point number's mantissa (including explicit bit), hiding the implementation detail.
Furthermore, this expression is typed with a sensible type, `intw<X, false>`, where X (the integer's size) depends on the underlying floating point format.
Using this expression with other integers will be type checked so a lot of errors can be caught at compile time.

`intw` is based on Xilinx's arbitrary precision types (`ap_int`) and `floatw` is based on the language's floating point number and supports the [half library](http://half.sourceforge.net/).
`floatw` could be easily extended to support custom implementations.
Most of the operators are simply forwarded to the underlying implementation.

The use of wrappers should be transparent for most operations (e.g. `a * b` does what you think it does).
A few extra operators are implemented to suit Expect's needs.

The wrappers really do three major things:
- add an object oriented interface
- add type safety to operators
- implement a few extra operators/setters/getters


Build instructions
------------------

### C++ models

Expect can be used as a regular C++ class to test it, debug it or improve it.
A set of tests are available. Building them requires:
- meson
- ninja (or any meson's backend)
- Xilinx's arbitrary precision types library
- half library
- MPFR library
- GoogleTest framework

To build automatically fetch all dependencies and build with Nix: `nix-build -A expect-models`.
Then run the tests binaries in `./result/bin/`

To build iteratively (repeat only last step when changing source):
```sh
nix-shell -A expect-models # or install all dependencies yourself
mkdir build
cd build
meson ../src
ninja
```

To run the tests, run one or the other:
```sh
meson test
./tests-expect # run only expect tests and directly show output
```
The tests binaries has some options (in particular to select which tests to run), use `-h` to see them.

The IPs (see below) simulation code can also be built (without Vivado, with a regular compiler) with:
```sh
nix-build -A ips.ips.propagate.Ffloat-B565-W60-R1-Z298.sim
```

### IPs

For several reasons (license, size, difficulty, ease of usage without Nix), Vivado is not packaged in Nix. Instead we use a mixed approach:
- Nix is used to generate a TCL script
- the TCL script can be used as usual

This has several advantages:
- using Nix abstraction to handle the IPs
- the generated TCL scripts are portable
- the machine with Vivado may not have Nix installed
- we could assume that if a TCL script is present in the store the IP had been generated
- the IPs can be generated without Nix without too much hassle

However, it has drawbacks:
- can't use Nix to conveniently manage dependencies (GTest, MPFR, awk, etc)
- can't provide tools with a known (and recent!) version (env supporting -S, etc)
- can't use Nix to manage parallel build/remote builders
- it's harder to avoid building several times the same IP on the exact same sources, while it's trivial with Nix
- reproducibility is not ensured at all
- etc

To build all IPs scripts:
```sh
nix-build -A ips.ips-list
```

To build a specific IP:
```sh
nix-build -A ips.ips.propagate.Ffloat-B565-W60-R1-Z298
```

Then do:
```sh
cp -r result/ build
chmod -R +w build
cd build
./synthesis.tcl
./impl.tcl
```

To build an IP without Nix:
- use `src/ip/$ip/hw.cpp` as hardware file, `src/ip/$ip/sim.cpp` as testbench file
- use `src/ip/$ip/hw.h` and headers in `src/inc`
- define `expect_*` constants used in these files
