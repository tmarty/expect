{ pkgs ? import <nixpkgs> {} }:

let
  packages = self: with self; {
    inherit (pkgs) lib;

    # like writeScriptBin without /bin prefix
    writeScriptDir = name: text: pkgs.writeTextFile {
      inherit name text;
      executable = true;
      destination = "/${name}";
    };

    # Generate a unique hash from a derivation without path separator (/)
    hash = drv: builtins.unsafeDiscardStringContext
      (builtins.hashString "sha1" drv.drvPath);

    ap_types = callPackage ./nix/ap_types.nix {};

    expect-models = callPackage ./nix/expect-models.nix {
      # Vivado HLS use , clang: using clang in the C++ models saves time because
      # gcc and clang may not have the same pedantry.
      # However, Vivado HLS 2019.1 still uses clang 3.9…
      stdenv = pkgs.clangStdenv;
    };

    checkII = callPackage ./nix/checkii.nix {};

    mkreport = callPackage ./nix/mkreport.nix {};

    mkIP = callPackage ./nix/mkip.nix {
      vitis = "/opt/Xilinx/Vitis_HLS/2020.2";
    };

    mkExpectIP = lib.makeOverridable (
      { project
      , config
      , cflags ? []
      , paths ? []
      , simAttrs ? {}
      , ... } @ args:
      mkIP ({
        inherit project;
        solution =
            "F${config.basefloat}"
          + "-B${toString config.B}"
          + "-W${toString config.W}"
          + "-R${toString config.R}"
          + "-Z${toString config.Zero}";
        src = pkgs.symlinkJoin {
          name = "src";
          paths = paths ++ [
            ./src/inc
            (./src/ip + ("/" + project))
          ];
        };
        files = [ "hw.cpp" ];
        testfiles = [ "sim.cpp" ];
        cflags = [
          "-Dexpect_B=${toString config.B}"
          "-Dexpect_W=${toString config.W}"
          "-Dexpect_R=${toString config.R}"
          "-Dexpect_Zero=${toString config.Zero}"
          "-Dexpect_basefloat=${config.basefloat}"
          "-std=c++11"
        ] ++ cflags;
        top = "hw_toplevel";
        # part = "xc7z045ffg900-2"; # zc706
        part = "XC7Z020-CLG484-1"; # zc702
        period = "200MHz";
        simAttrs = simAttrs // {
          buildInputs = (simAttrs.buildInputs or []) ++ [
            pkgs.half
            ap_types
          ];
        };
      } // (
        removeAttrs args [
          "project"
          "B" "W" "R" "Zero" "config"
          "cflags"
          "path"
          "simAttrs"
        ]
      ))
    );

    ips = rec {
      # These configs support 2^acc accumulations of basefloat products
      configs =
      let
        pow = b: p: if p == 0 then 1 else b * (pow b (p - 1));
        mkcfg = { basefloat, we, wf }: acc: R: {
          inherit basefloat R;
          B = ((acc + pow 2 (we + 1) + 2 * wf - 3) + R - 1) / R;
          W = acc + 2 * (wf + 2);
          Zero = pow 2 we + 2 * wf - 4;
        };
        half   = { basefloat = "half";   we = 5;  wf = 10; };
        single = { basefloat = "float";  we = 8;  wf = 23; };
        double = { basefloat = "double"; we = 11; wf = 52; };
      in [
        (mkcfg half 10 1)
        # (mkcfg half 10 2)
        # (mkcfg half 10 4)
        (mkcfg single 10 1)
        (mkcfg single 10 2)
        # (mkcfg single 10 4)
        # (mkcfg single 10 8)
        (mkcfg double 10 1)
        # (mkcfg double 10 2)
        # (mkcfg double 10 4)
        # (mkcfg double 10 8)
      ];

      projects = [
        "accumulate-expect"
        "accumulate-float"
        "compare"
        "mac-float-float"
        "propagate"
        # "mmult-expect"
      ];

      # all IPs as list (useful to build them all or iterate)
      ips-list = map mkExpectIP (lib.cartesianProductOfSets {
        project = projects;
        config = configs;
      });

      # all IPs as attribute set (useful to build a specific one)
      # IPs are organized like: ips.${project}.${solution}
      ips = lib.groupBy'
        (a: ip: a // { ${ip.solution} = ip; }) {}
        (ip: ip.project)
        ips-list;

      # dynamically generate a .gitlab-ci.yml file from information in this file
      # Note: need a recent gitlab-runner to avoid race condition on artifact
      # downloads when using shell executor
      gitlab-ci = pkgs.writeText "gitlab-ci.yml" (lib.generators.toYAML {} ({
        stages = [
          "sim"
          "prepare"
          "synthesis"
          "impl"
          "report"
        ];

        prepare = {
          stage = "prepare";
          tags = [ "nix" ];
          artifacts.paths = map hash ips-list;
          before_script = [ ''. "$HOME/.nix-profile/etc/profile.d/nix.sh"'' ];
          script = lib.concatMap (ip: [
            "nix-build -A ips.ips.${ip.project}.${ip.solution}"
            "cp -rL result/ ${hash ip}"
            "chmod -R +w ${hash ip}"
          ]) ips-list;
        };

        report = {
          stage = "report";
          tags = [ "nix" ];
          when = "always"; # make a report even if some impl are skipped / failed
          artifacts.paths = [ "report.csv" ];
          dependencies = map (ip: "impl:${ip.project}:${ip.solution}") ips-list;
          before_script = [ ''. "$HOME/.nix-profile/etc/profile.d/nix.sh"'' ];
          script = [
            "nix-build -A mkreport"
            "./result report.csv"
          ];
        };
      } // (lib.foldl' (a: b: a // b) {} (map (ip: {
        "sim:${ip.project}:${ip.solution}" = {
          stage = "sim";
          tags = [ "nix" ];
          before_script = [ ''. "$HOME/.nix-profile/etc/profile.d/nix.sh"'' ];
          script = [ "nix-build -A ips.ips.${ip.project}.${ip.solution}.sim" ];
        };

        "synthesis:${ip.project}:${ip.solution}" = rec {
          stage = "synthesis";
          tags = [ "vivado" ];
          needs = [ "prepare" ];
          cache = {
            key = "${hash ip}-synthesis";
            paths = map (p: "cache/" + p) ([ "jobid" "joburl" ] ++ artifacts.paths);
            when = "always"; # save the cache on success and failure
          };
          artifacts.paths = [ (hash ip) ];
          script = [
            ''
              if test -d cache
              then
                echo "This job already ran in CI job #$(cat cache/jobid): $(cat cache/joburl)"
                grep -q success cache/status
                cp -r cache/${hash ip}/* ${hash ip} # for artifacts
                exit 0
              fi
            ''
            "pushd ${hash ip}"
            "./synthesis.tcl -l vivado_hls_synthesis.log && status=success || status=failed"
            "popd"
            "mkdir -p cache"
            "echo $CI_JOB_ID > cache/jobid"
            "echo $CI_JOB_URL > cache/joburl"
            "echo $status > cache/status"
            "cp -r ${hash ip} cache/"
            "test $status = success"
          ];
        };
      } // lib.optionalAttrs ip.impl {
        "impl:${ip.project}:${ip.solution}" = rec {
          stage = "impl";
          tags = [ "vivado" ];
          when = "manual";
          needs = [ "synthesis:${ip.project}:${ip.solution}" ];
          cache = {
            key = "${hash ip}-impl";
            paths = map (p: "cache/" + p) ([ "jobid" "joburl" ] ++ artifacts.paths);
            when = "always";
          };
          artifacts.paths = [
            "${hash ip}/*.log"
            "${hash ip}/${ip.project}/${ip.solution}/syn/report"
            "${hash ip}/${ip.project}/${ip.solution}/impl/report"
          ];
          script = [
            ''
              if test -d cache
              then
                echo "This job already ran in CI job #$(cat cache/jobid): $(cat cache/joburl)"
                grep -q success cache/status
                cp -r cache/${hash ip}/* ${hash ip} # for artifacts
                exit 0
              fi
            ''
            "pushd ${hash ip}"
            "./impl.tcl -l vivado_hls_impl.log && status=success || status=failed"
            "popd"
            "mkdir -p cache"
            "echo $CI_JOB_ID > cache/jobid"
            "echo $CI_JOB_URL > cache/joburl"
            "echo $status > cache/status"
            "cp -r ${hash ip} cache/"
            "test $status = success"
          ];
        };
      }) ips-list))));
    };
  };
in
  pkgs.lib.makeScope pkgs.newScope packages
