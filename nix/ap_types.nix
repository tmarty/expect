{ stdenv, lib, fetchFromGitHub }:

stdenv.mkDerivation rec {
  name = "arbitrary_precision_types";

  src = fetchFromGitHub {
    owner = "Xilinx";
    repo = "HLS_arbitrary_Precision_Types";
    rev = "200a9aecaadf471592558540dc5a88256cbf880f";
    sha256 = "1hjxkif5siqpflkcls27sbngfawljrsi2j6l2ch46rahl7pfglcf";
  };

  installPhase = ''
    mkdir $out
    cp -r include $out
  '';

  meta = with lib; {
    homepage = https://www.xilinx.com/;
    description = "HLS Arbitrary Precision Types Library";
    longDescription = ''
      This provides simulation code of HLS Arbitrary Precision Types.
      The code is based from headers shipped with Vivado, but due to absence of synthesis support it should not be used in an HLS project targeting FPGA.
    '';
    license = licenses.asl20;
  };
}
