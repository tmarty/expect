{ writeTextDir }:

# This script inspects Vivado HLS synthesis log to check for II violation
# Note: target II is missing from XML reports, so we use the log
# Quotes with awk (BEGIN block) ref: https://stackoverflow.com/a/43213844

# Note: do not use #! ${gawk}/bin/awk -f to be portable…
# Note: my server with Vivado has an old env which does not support -S, so we
# can't use '#! /usr/bin/env -S gawk -f' shebangs. Instead, we write a non
# executable file and call it with gawk -f

writeTextDir "checkII.awk" ''
  BEGIN {
    FPAT = "([^ ]+)|('[^']+')"
    failed = 0
    print "Checking for II violations"
  }
  /INFO: \[HLS 200-1470\] Pipelining result/ {
    target = strtonum($10)
    final  = strtonum($14)
    mode   = $18
    name   = $19
    if(final > target) {
      failed = 1
      printf "Scheduler failed to meet II constraint for %s %s: %d -> %d\n",
        mode,
        name,
        target,
        final
    }
    else {
      printf "Scheduler meets II constraint for %s %s: %d\n",
        mode,
        name,
        final
    }
  }
  END {
    if(failed) exit 1
  }
''
