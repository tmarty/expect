{ stdenv, lib, meson, ninja, pkg-config
, ap_types, gtest, mpfr, half }:

stdenv.mkDerivation {
  name = "expect-models";

  src = ../src;

  nativeBuildInputs = [
    meson
    ninja
    pkg-config
  ];

  buildInputs = [
    ap_types
    gtest
    mpfr
    half
  ];

  mesonBuildType = "debugoptimized";

  # doCheck = true; # just build

  outputs = [ "out" "logs" ];

  postInstall = ''
    mkdir $logs
    cp -r meson-logs/* $logs
  '';
}
