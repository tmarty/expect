{ lib, symlinkJoin, writeScriptDir, checkII, vitis
, clangStdenv, meson, ninja
}:

with lib;

let defaultVitis = vitis; in

{ top
, project ? top
, solution ? project
, name ? (project + "-" + solution)
, src
, files
, testfiles ? []
, cflags ? []
, ldflags ? []
, part
, period
, csim ? (testfiles != [])
, cosim ? (testfiles != [])
, simArgv ? []
, extraCommands ? []
, impl ? true
, implFormat ? "ip_catalog"
, rtl ? "vhdl"
, doCheckII ? true
, vitis ? defaultVitis
, simAttrs ? {}
}:

let
  cflagsStr    = concatStringsSep " " cflags;
  ldflagsStr   = concatStringsSep " " ldflags;
  simArgvStr   = concatStringsSep " " simArgv;
  filesStr     = concatStringsSep " " files;
  testfilesStr = concatStringsSep " " testfiles;

  # A derivation to compile source files with a regular toolchain.
  # - if csim or cosim is true, it builds an executable and run the tests
  # - otherwise, it just compiles the files (in a library, quick solution
  #   because there's no main/_start)
  mesonListToString = concatMapStringsSep ", " (s: "'" + s + "'");
  sim = clangStdenv.mkDerivation (simAttrs // {
    name = name + "-sim";

    inherit src;

    nativeBuildInputs = (simAttrs.nativeBuildInputs or []) ++ [
      meson
      ninja
    ];

    doCheck = true;

    postPatch = (simAttrs.postPatch or "") + ''
      cat > meson.build << EOF
      project('${name}-sim', 'cpp', version: '1')
      add_project_arguments(['-Wno-unknown-pragmas', '-Wno-unused-label'], language: 'cpp')
      sim = ${if (cosim || csim) then "executable" else "library"}('sim',
        ${mesonListToString (files ++ testfiles)},
        include_directories: include_directories('${../src/compat}'),
        cpp_args: [${mesonListToString cflags}],
        link_args: [${mesonListToString ldflags}],
        install: true
      )
      ${optionalString (cosim || csim) "test('sim', sim, args: [${mesonListToString simArgv}])"}
      EOF
    '';
  });

in symlinkJoin {
  inherit name;
  paths = [
    src
    (writeScriptDir "synthesis.tcl" ''
      #! ${vitis}/bin/vitis_hls -f

      open_project ${project}

      set_top ${top}
      add_files -cflags {${cflagsStr}} "${filesStr}"
      ${optionalString (testfiles != []) "add_files -tb -cflags {${cflagsStr}} \"${testfilesStr}\""}
      open_solution ${solution} -reset
      set_part ${part}
      create_clock -period ${period} -name default

      ${concatStringsSep "\n" extraCommands}

      ${optionalString csim "csim_design -ldflags {${ldflagsStr}} -argv {${simArgvStr}}"}

      csynth_design

      ${optionalString doCheckII "exec gawk -f ./checkII.awk [glob *.log] >@stdout"}

      ${optionalString cosim "cosim_design -ldflags {${ldflagsStr}} -argv {${simArgvStr}}"}

      exit
    '')
  ] ++ (optional impl (
    writeScriptDir "impl.tcl" ''
      #! ${vitis}/bin/vitis_hls -f

      open_project ${project}

      open_solution ${solution}

      export_design -flow impl -rtl ${rtl} -format ${implFormat}

      exit
    ''
  )) ++ (optional doCheckII checkII);

  passthru = {
    inherit sim;
    # provide given arguments back
    # Note: @args misses arguments with default value if they are not explicit
    inherit
      top
      project
      solution
      name
      src
      files
      testfiles
      cflags
      ldflags
      part
      period
      csim
      cosim
      simArgv
      extraCommands
      impl
      implFormat
      rtl
      doCheckII
      vitis
      ;
  };
}
