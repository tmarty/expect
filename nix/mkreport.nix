{ python3, writeText, writeShellScript }:

# Build a CSV file summarizing metrics for several IPs from their synthesis and
# export reports file

let
  python = python3.withPackages (ps: with ps; [ xmltodict ]);
  script = writeText "readVivadoReports.py" ''
    import sys, xmltodict

    paths = sys.argv[1].split('/')
    project = paths[-3]
    solution = paths[-2]
    print("{},{}".format(project, solution), end="")

    with open(sys.argv[2]) as fd:
      dict = xmltodict.parse(fd.read())
      resources = dict['profile']['AreaReport']['Resources']
      available = dict['profile']['AreaReport']['AvailableResources']
      for r in ['SLICE', 'LUT', 'FF', 'DSP', 'BRAM']:
        print(",{},{},{:%}".format(resources[r], available[r], float(resources[r]) / float(available[r])), end="")
      targetclock = dict['profile']['TimingReport']['TargetClockPeriod']
      achievedclock = dict['profile']['TimingReport']['AchievedClockPeriod']
      print(",{},{}".format(targetclock, achievedclock), end="")
    with open(sys.argv[3]) as fd:
      dict = xmltodict.parse(fd.read())
      latency = dict['profile']['PerformanceEstimates']['SummaryOfOverallLatency']['Worst-caseLatency']
      interval = dict['profile']['PerformanceEstimates']['SummaryOfOverallLatency']['Interval-max']
      print(",{},{}".format(latency, interval))
  '';
  header =
    "projet,solution," +
    "used SLICE,available SLICE,SLICE%," +
    "used LUT,available LUT,LUT%," +
    "used FF,available FF,FF%," +
    "used DSP,available DSP,DSP%," +
    "used BRAM,available BRAM,BRAM%," +
    "target clock,achieved clock," +
    "latency,interval";
in writeShellScript "mkreport" ''
  out=''${1:-report.csv}
  echo "${header}" > $out
  find . -name impl -print0 | while IFS="" read -r -d "" d
  do
    ${python}/bin/python ${script} "$d" "$d/report/"*/*"_export.xml" "$d/../syn/report/csynth.xml" >> $out
  done
''
