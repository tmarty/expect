#ifndef __HLS_HALF_H_
#define __HLS_HALF_H_

// Compatibility file between Xilinx half and upstream half

#include <half.hpp>

using namespace half_float;

#endif // __HLS_HALF_H_
