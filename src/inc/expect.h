#ifndef __EXPECT_H
#define __EXPECT_H

#include <type_traits>
#include <iomanip>
#include <cassert>
#include "floatwrapper.h"
#include "intwrapper.h"

/*
 * Expect accumulator class
 * Template arguments:
 *  - B: number of lines
 *  - W: width of each line
 *  - R: redundance rate
 *  - Zero: position of weight zero (0 <= Zero < B×R)
 *
 * /!\ the thesis manuscript use different notations:
 * +--------+--------+
 * | thesis |  code  |
 * +--------+--------+
 * | W      |  B × R |
 * | w      |  W     |
 * | r      |  R     |
 * | Z      |  Zero  |
 * +--------+--------+
 */

template <unsigned int _B, unsigned int _W, unsigned int _R, unsigned int _Zero>
class expect {
  private:
    typedef expect<_B, _W, _R, _Zero> this_t;

    // Compute base 2 logarithm (works on integer types), rounded up or down
    template <typename T>
    static constexpr T floorlog2(T x) {
      return (x == 1) ? 0 : 1 + floorlog2(x >> 1);
    }
    // This is also number of bits needed to represent at most x (excluded)
    // e.g if x = 16, how many bits are needed to represent 0..15
    template <typename T>
    static constexpr T ceillog2(T x) {
      return (x == 1) ? 0 : floorlog2(x - 1) + 1;
    }

  public:
    constexpr static unsigned int B = _B;
    constexpr static unsigned int W = _W;
    constexpr static unsigned int R = _R;
    constexpr static unsigned int Zero = _Zero;

    constexpr static int wmax = B * R - Zero;
    constexpr static int wmin = - Zero;

    typedef sintw<W> word_t;
    typedef uintw<ceillog2(B)> line_t;
    typedef uintw<B * R> fixedpoint_t; // zero position is implicit
    typedef uintw<ceillog2(B * R)> bit_t;
    // unfortunately std::max is not constexpr in VHLS
    // typedef sintw<ceillog2(std::max(wmax, Zero)) + 1>
    typedef sintw<ceillog2((wmax > Zero) ? wmax : Zero) + 1>
      signed_bit_t;
    typedef uintw<ceillog2(R)> significant_shift_t;

    const sintw<bit_t::width> zero = Zero;

  private:
    word_t words[B];

    enum class FPClass {
      Normal,
      PositiveInfinity,
      NegativeInfinity,
      NaN,
    } fpclass;

  public:
    /* *** Low-level interface *** */
    expect()
      : fpclass(FPClass::Normal)
    {}

    word_t& get_line(const line_t& line) {
      // Note: widening line by 1 bit is necessary if B is a power of 2.
      // Otherwise, B is represented as zero in this type.
      assert(line.template widen<line_t::width + 1>() < B);

      return words[line.unravel()];
    }

    word_t get_line(const line_t& line) const {
      assert(line.template widen<line_t::width + 1>() < B);

      return words[line.unravel()];
    }

    word_t& set_line(const line_t& line, const word_t& word) {
      assert(line.template widen<line_t::width + 1>() < B);

      words[line.unravel()] = word;
      return words[line.unravel()];
    }

    uintw<W - R> get_line_overlap(const line_t& line) const {
      return get_line(line).template slice<W - 1, R>();
    }
    uintw<R> get_line_significant(const line_t& line) const {
      return get_line(line).template slice<R - 1, 0>();
    }

    // Return a bit as if the accumulator was a simple fixed point.
    // Bit position are aligned on zero (0 is 2^0).
    // /!\ precondition: propagate()
    uintw<1> get_bit(signed_bit_t weight) const {
      line_t line;
      significant_shift_t shift;
      weight = weight.modularAdd(zero);
      weight
        .as_unsigned()
        .template slice<bit_t::width - 1, 0>()
        .split(line, shift);
      return (get_line_significant(line) >> shift).template get<0>();
    }

    // Accumulate a word in a line (with no other logic)
    template<unsigned int W2, bool is_signed>
    typename std::enable_if<W2 <= W, this_t&>::type accumulate(const intw<W2, is_signed>& word, const line_t& line) {
#ifdef DEBUG
      std::cerr << "accumulate3(" << word << ", " << line << ")\n";
#endif

      set_line(line, get_line(line).modularAdd(word.template widen<W>().as_signed()));
      return *this;
    }

    // Accumulate a word in a bit position (determine line and shift)
    // Bit position is aligned on LSB (0 is LSB).
    // Note: this method is disabled if R = 1 as it is useless in this case
    // (and would have a conflicting signature because line_t = bit_t)
    // (and significant_shift_t would not make sense as there is no shift)
    template<unsigned int W2, bool is_signed>
    typename std::enable_if<(R != 1) && (W2 + R - 1 <= W), this_t&>::type accumulate(const intw<W2, is_signed>& word, const bit_t& weight) {
#ifdef DEBUG
      std::cerr << "accumulate2(" << word << ", " << weight << ")\n";
#endif

      line_t line;
      significant_shift_t shift;
      weight.split(line, shift);
      return accumulate(word << shift, line);
    }

    // Accumulate a word on a weight
    // The weight is aligned on Zero (0 is Zero)
    // The weight's word length (WL) can be arbitrary, narrower or bigger than
    // bit_t, so there is two implementations:
    // - if WL is larger than bit_t::width
    // - if WL is narrower (or equal) than bit_t::width
    // Note: if WL is equal than bit_t::width, the signatures are still
    // different because here the weight is signed
    template<unsigned int W2, bool is_signed, unsigned int WL>
    typename std::enable_if<(WL <= bit_t::width), this_t&>::type accumulate(const intw<W2, is_signed>& word, const sintw<WL>& weight) {
#ifdef DEBUG
      std::cerr << "accumulate1<=(" << word << ", " << weight << ")\n";
#endif

      // Add zero to get weight relative to LSB instead of Zero
      auto e = (weight + zero).template slice<bit_t::width - 1, 0>();
      return accumulate(word, e);
    }

    template<unsigned int W2, bool is_signed, unsigned int WL>
    typename std::enable_if<(WL > bit_t::width), this_t&>::type accumulate(const intw<W2, is_signed>& word, const sintw<WL>& weight) {
#ifdef DEBUG
      std::cerr << "accumulate1>(" << word << ", " << weight << ")\n";
#endif

      // If exponent is too large for the format: set to ±inf
      if(weight > wmax) { // weight centered around zero
        fpclass =
          word.sign() ?
          FPClass::NegativeInfinity :
          FPClass::PositiveInfinity;
      }
      // Exponent is valid
      else if(weight >= wmin) { // weight centered around zero
        // Add zero to get weight relative to LSB instead of Zero
        auto e = (weight + zero).template slice<bit_t::width - 1, 0>();
        accumulate(word, e);
      }
      // If exponent is too small, do nothing (like zero)

      return *this;
    }

    this_t& reset() {
      fpclass = FPClass::Normal;
      reset:for(unsigned int line = 0; line < B; line++)
        set_line(line, 0);
      return *this;
    }

    /* *** High-level interface *** */
    template <typename basefloat>
    this_t& accumulate(const floatw<basefloat>& f) {
#ifdef DEBUG
      std::cerr << "accumulate0(" << f << ")\n";
#endif

      auto e = f.real_exponent();
      auto m = f.signed_explicit_mantissa();

      // Subtract wf because the exponent is for the mantissa's MSB while we
      // accumulate the word based on its LSB' weight.
      auto ez = e - f.wfw();

      // Handle special cases
      // TODO: this logic should be factorized with other methods, and for rhs/lhs
      if(f.isnan()) {
        fpclass = FPClass::NaN;
      }
      else if(f.isinf()) {
        switch(fpclass) {
          case FPClass::Normal:
            fpclass = f.sign() ? FPClass::NegativeInfinity : FPClass::PositiveInfinity;
            break;
          case FPClass::PositiveInfinity:
            fpclass = f.sign() ? FPClass::NaN : FPClass::PositiveInfinity;
            break;
          case FPClass::NegativeInfinity:
            fpclass = f.sign() ? FPClass::NegativeInfinity : FPClass::NaN;
            break;
          case FPClass::NaN:
            break;
        }
      }
      else // normal case
        accumulate(m, ez);

      return *this;
    }

    // Accumulate another Expect
    // Accumulate the *full* lines, not only the significant bits (which is a
    // waste of resources if any had been propagated)
    template <unsigned int B2, unsigned int W2, unsigned int Zero2>
    typename std::enable_if<
      (W2 <= W) &&
      (Zero2 <= Zero) &&
      (B2 - Zero2 <= B - Zero),
      this_t&>::type
    accumulate(const expect<B2, W2, R, Zero2>& e) {
      accumulate_expect:for(unsigned int line = 0; line < B; line++) {
#pragma HLS pipeline
        set_line(
          line,
          (get_line(line) + e.get_line(line))
            .template slice<W - 1, 0>()
            .as_signed()
        );
      }

      return *this;
    }

    template <typename basefloat>
    this_t& mac(const floatw<basefloat>& lhs, const floatw<basefloat>& rhs) {
#ifdef DEBUG
      std::cerr << "mac0(" << lhs << ", " << rhs << ")\n";
#endif

      auto el = lhs.real_exponent();
      auto ml = lhs.signed_explicit_mantissa();
      auto er = rhs.real_exponent();
      auto mr = rhs.signed_explicit_mantissa();

      auto e = el + er;
      auto m = ml * mr;

      // Subtract 2×wf because the exponent is for the mantissa's MSB while we
      // accumulate the word based on its LSB's weight.
      auto ez = e - lhs.wfw() - rhs.wfw();

      // Handle special cases
      if(lhs.isnan() || rhs.isnan()) {
        fpclass = FPClass::NaN;
      }
      else if(lhs.isinf() && rhs.isinf()) {
        fpclass =
          (lhs.sign() == rhs.sign()) ?
          FPClass::PositiveInfinity :
          FPClass::NegativeInfinity;
      }
      else if((lhs.isinf() && rhs.iszero()) || (rhs.isinf() && lhs.iszero())) {
        fpclass = FPClass::NaN;
      }
      else if(lhs.isinf()) {
        switch(fpclass) {
          case FPClass::Normal:
            fpclass = lhs.sign() ? FPClass::NegativeInfinity : FPClass::PositiveInfinity;
            break;
          case FPClass::PositiveInfinity:
            fpclass = lhs.sign() ? FPClass::NaN : FPClass::PositiveInfinity;
            break;
          case FPClass::NegativeInfinity:
            fpclass = lhs.sign() ? FPClass::NegativeInfinity : FPClass::NaN;
            break;
          case FPClass::NaN:
            break;
        }
      }
      else if(rhs.isinf()) {
        switch(fpclass) {
          case FPClass::Normal:
            fpclass = rhs.sign() ? FPClass::NegativeInfinity : FPClass::PositiveInfinity;
            break;
          case FPClass::PositiveInfinity:
            fpclass = rhs.sign() ? FPClass::NaN : FPClass::PositiveInfinity;
            break;
          case FPClass::NegativeInfinity:
            fpclass = rhs.sign() ? FPClass::NegativeInfinity : FPClass::NaN;
            break;
          case FPClass::NaN:
            break;
        }
      }
      else // normal case
        accumulate(m, ez);

      return *this;
    }

  private:
    // Subfunction for propagate(): find a candidate in sout's absolute value
    template <typename basefloat>
    void find_candidate(
      const bool& neg,
      typename floatw<basefloat>::sign_t& s,
      const uintw<floatw<basefloat>::wf() + R>& usout,
      typename floatw<basefloat>::explicit_mantissa_t& m,
      const unsigned int& line,
      signed_bit_t& e
    ) {
      // Position of smallest normal bit (counting from the accumulator LSB)
      // i.e., boundary between normal bits and subnormal ones
      constexpr int boundary = Zero - floatw<basefloat>::bias() + 1;
      // Position of the boundary in a block of size R (counting… idem)
      constexpr int rboundary = boundary % R;

      // usout R MSB
      auto usoutmsb = usout
        .template slice<
          floatw<basefloat>::wf() + R - 1,
          floatw<basefloat>::wf()>();

      // Do we need to save the subnormal candidate? Yes if:
      // - usout's R MSB contains the boundary bit (it would be the
      //   implicit bit).
      // - bits above the normal boundary (included) are zero
      bool subnormal =
        (line == boundary / R + 1) &&
        usoutmsb.template slice<R - 1, rboundary>().nor_reduce();

      // Otherwise, do we have a candidate? Yes if usout's R MSB contains a 1
      bool candidate = bool(usoutmsb.or_reduce());

      if(subnormal || candidate) {
        // Implicit bit 1 can have multiple position if R>1: we use the most
        // significant one. We need to shift usout accordingly to get the
        // mantissa. The shift is statically known in subnormal case.
        auto shift = uintw<R>::shift_t::mux(
          subnormal,
          rboundary,
          usoutmsb.lop()
        );

        // Get candidate's mantissa
        m = (usout >> shift)
          .template slice<floatw<basefloat>::wf(), 0>();

        // Get candidate's exponent
        // line-1 is the line in which the implicit bit was in the R LSB
        e = bit_t((line - 1) * R)
          .modularAdd(shift) // correct exponent based on the implicit bit position
          .as_signed()
          .modularSub(zero) // set value around Zero
          .template widen<signed_bit_t::width>();

        // Get candidate's sign
        s = neg;

#ifdef DEBUG
        // Debug info
        std::cerr
          << "candidate "
          << (subnormal ? "(subnormal) " : "")
          << "at line=" << line
          << " (" << s << ' '
          << e << ' '
          << m.toHexString() << ")\n";
#endif
      }
    }

  public:
    template <typename basefloat = float>
    floatw<basefloat> propagate() {
      // This function does 2 things:
      // (1) it removes redundancy by propagating lines' carries such that each
      // lines has only R+1 significant bits. This does NOT change the
      // accumulator's value.
      // (2) it computes the accumulator's value in the basefloat format.
      //
      //
      // To do (1), it computes an accumulation of all lines and get some bits
      // from it to write back to the lines. To compute the accumulation, it
      // scans lines from LSB (0) to MSB (B-1) and computes the accumulation in
      // a "sliding window", the acc variable. W bits are just enough.
      // For each line, acc is shifted right by R positions (moving the sliding
      // window left by R positions, or from another point of view: aligning
      // acc's bits with the current line's ones) and the line is accumulated
      // into it. That's it. This works even if some lines are negative,
      // without taking special measures.
      //
      // When shifting acc, the bits "going out" have a nice property: they
      // will never change (while other bits can change due to accumulations of
      // the following lines). Therefore, they can be used to get the bits to
      // write back into the line (they are used for (2) too).
      // The basic idea is to simply write acc's R LSB into the current line.
      // They are exception through because of negative values: if we do that,
      // we would get 1's bits in all lines corresponding to the repeating sign
      // bits of the negative value. This would change the accumulator's value!
      // Instead, we want to write a -1 into the right line. For instance, the
      // value -10 can be represented as -2^4 +2^2 +2^1. This is, in fact, just
      // like its two's complement representation: …11110110 is indeed equal to
      // …11110000 (-16) + 0100 (4) + 0010 (2).  Thus, after writing 1 in the
      // line corresponding to 2 and 4, we write -1 in the line corresponding
      // to 16 (or equivalently if R>1).
      //
      // However, if a non zero line is accumulated later, then all those 1
      // bits where not sign bits but significant bits! But we wrote -1 in a
      // previous line instead of keep writing the significant 1's.
      // To fix this, we just have to write a 1 to "cancel" the -1 and keep the
      // accumulator's correct value.
      // For instance, let's say that we wrote 2, 4, and -16, but in a later
      // line we have the value 1280. Or accumulator (the caret shows sliding
      // window's LSB) would look like:
      //       1111111110110 = -10
      //      +0010100000000 =  1280
      //      =0010011110110 =  1270
      //           ^
      // If we just write 1024, then the accumulator's value would be wrong:
      // 1024 -16 +4 +2 = 1014.
      // By writing a 1 at 256, we get the correct result (1014 + 256 = 1270).
      // The 256 "cancels" the -16, and the value is the same as if we had
      // written the significant 1's. Indeed, these 4 bits (011110000) represent
      // a value of 240, which is equal to 256 - 16 (…0100000000 + …1111110000)
      // In the general case (with R>1), we must in fact write acc's R LSB
      // incremented by 1. Note that this sum cannot overflow because acc's R
      // LSB contains at least one 0 at this point.
      //
      // To sum up, the written value is determined by this state machine:
      //      ┌─────────────────────────────────────────────────────┐
      //      │         acc==-1                acc[R-1:0]≠-1        │
      //      └─▶(base)────────▶(neg)──▶(zero)──────────────▶(fix)──┘
      //                          │                            ▲
      //                          └────────────────────────────┘
      //                                   acc[R-1:0]≠-1
      //      (base) write acc[R-1:0]
      //      (neg)  write -1
      //      (zero) write 0
      //      (fix)  write acc[R-1:0] + 1
      // State (a) is the basic case. State (b) writes the -1 instead of keep
      // writing sign bits. There is no significant bits afterwards, thus state
      // (c) writes 0. However, if a non zero line is accumulated later
      // (negative or positive), we correct the mistake in state (d) and go
      // back to state (a). State (c) can be short-circuited if the condition
      // from (c) to (d) is already true at state (b).
      //
      //
      // To do (2), we look at the bits "shifting out" of the accumulator
      // sliding window, in the variable sout, which cannot be modified by
      // further accumulations. These bits can contain a "candidate" result.
      // In that case, we save the candidate's fields. The most significant
      // candidate is saved last and "wins".
      //
      // Because the floating point format uses a sign-magnitude format while
      // the lines use two's complement format, we need to compute sout's
      // absolute value. If a number x is negative, its absolute value can be
      // obtained by computing its two's complement: ~x+1. However, because we
      // only have a sliding window over a large (virtual) word, computing
      // ~sout+1 can be wrong. We need to know if the "carry-in" bit is 0 or 1.
      // It depends on the history of sout's shifted out bits (a 1 absorbs the
      // carry).
      // Once we have the absolute value, we look for a 1 bit that can be the
      // "implicit bit" of the candidate at a specific place in it. If we find
      // it, we save the candidate's sign, mantissa (implicit bit + bits
      // following the implicit bit) and exponent. Note that the candidate's
      // exponent can be outside of the format's field range.
      // After the loop, we look at the exponent and return an appropriate
      // value whether it is in the format's field range or not.
      //
      // sout needs wf + R bits. The implicit bit can be anywhere in the R MSB
      // bits. For R=1, the implicit bit is at a fixed position, like the
      // mantissa. For R>1, the implicit bit can be at several positions and
      // the mantissa must be shifted accordingly.
      //
      // In addition to this case, we must also save the subnormal candidate.
      // This happens at a fixed static position in the accumulator.
      //
      // To distinguish between the subnormal and the normal case in the result
      // generation phase, we must save the implicit bit (0 or 1). Indeed, a
      // subnormal and a normal can have the same exponent (-bias + 1) so we
      // need a bit (boolean) to make the difference. The implicit bit
      // detection takes precedence over the subnormal case.
      //
      // There is one special case, the mirror of the sign bits "becoming"
      // significant as explained in (1). Here, significant bits can "become"
      // sign bits, which would result in a wrong result if we don't take care
      // of it. Consider this example (with R=1): the line 2^5 has a value of
      // -1 (-32) and the 5 next lines have a value of 1 (16, 8, 4, 2, 1).
      // When propagating, those 1 are considered significant, resulting in a
      // sequence of candidates: 1, 3, 7, and 15 (the last 1 does not make a
      // candidate (31) because at this point acc=-1 (-32): we enter (neg)
      // state and start looking for candidate in sout's two's complement, not
      // sout). But these bits are in fact scattered sign bits.
      // The problem is that the last candidate will be 15, with an exponent of
      // 3, while the correct result is -1, with an exponent of 0. So, in this
      // case, when we know that the bits were in facts only sign bits we've
      // lost the sout value (and the exponent) allowing us to compute the
      // correct candidate.
      //
      // We fix this issue in two steps: capture and resolution.
      // We save (capture) the current line (to get the candidate's exponent)
      // and current sout's two's complement (to get the candidate's mantissa
      // when we sout's two's complement R MSB contains a 1. (This corresponds
      // to the previous example, but without loss of generality.) This 1 will
      // be the candidate's implicit bit. Just like the candidates, the last
      // captured value "wins".
      // When we enter (neg) state, we save the candidate from the captured
      // value (resolution).
      // case either:
      //
      // If the significant bits that we considered as sign bits were in fact
      // significant bits it works:
      // - if we never go to (neg) state, there is no resolution
      // - if we go to (neg) state not *right after* the sign bits (i.e., they
      //   are significant), then at least one of sout's two's complement R MSB
      //   bits is 1 (the lack of significant/sign bits put 0 in sout), and
      //   capture takes precedence over resolution.
      // In both case there is no resolution, so the corresponding candidate is
      // not generated.
      //
      //
      // Note that while the accumulator can contain a total of B×R + W-R non
      // redundant bits (because of the last line overlap bits), it is only
      // guaranteed to work on B×R bits. In particular, if any bit above the
      // weight B×R is set, it will be lost and the accumulator's value will
      // change (this is an overflow) and those bits will not be taken in
      // account for the normalisation.

      // Sliding window over the accumulator
      word_t acc(0);

      // Bits "shifting out" of acc
      uintw<floatw<basefloat>::wf() + R> sout(0);
      // sout's two's complement
      uintw<floatw<basefloat>::wf() + R> nsout;

      // Carry-in for absolute value computation (aka sticky bit)
      uintw<1> carryin(1);

      // Captured values for the sign/significant bits special case
      unsigned int captured_line(0);
      uintw<floatw<basefloat>::wf() + R> captured_nsout(0);
      bool resolution(false);

      // Candidate's "extended" fields
      signed_bit_t e(0);
      typename floatw<basefloat>::explicit_mantissa_t m(0);

      // Candidate's fields
      typename floatw<basefloat>::sign_t fs(0);
      typename floatw<basefloat>::exponent_t fe(0);
      typename floatw<basefloat>::mantissa_t fm(0);

      // State to know what to write back to the lines
      enum class State {
        base,
        neg,
        zero,
        fix
      } state(State::base);

      // Note: we add 1 iteration after the last line (B-1) to find candidate
      // in the very last shifting out bits, from the last line
      propagate:for(unsigned int line = 0; line < (B + 1); line++) {
#pragma HLS pipeline
        // Whether the current iteration has a valid line
        bool valid = line < B;

        // Compute sliding window shift in 3 steps (from LSB to MSB)
        // - carry-in bit
        // - shifting out bits
        // - accumulator itself

        carryin = carryin & sout.template slice<R - 1, 0>().nor_reduce();

        sout = sout.linsert(acc.template slice<R - 1, 0>());

        acc = acc >> typename word_t::shift_t(R);

        // accumulate with current line's R LSB
        if(valid)
          acc = acc.modularAdd(get_line(line));

        // State machine step
        switch(state) {
          case State::base:
            if(acc.and_reduce())
              state = State::neg;
            break;
          case State::neg:
            if(acc.template slice<R - 1, 0>().nand_reduce())
              state = State::fix;
            else
              state = State::zero;
            break;
          case State::zero:
            if(acc.template slice<R - 1, 0>().nand_reduce())
              state = State::fix;
            break;
          case State::fix:
            state = State::base;
            break;
        }

        // Shortcut: whether the current value in sout must be considered
        // negative
        bool neg = (state == State::neg) || (state == State::zero);

        // Write back to the current line
        if(valid) {
          switch(state) {
            case State::base:
              set_line(
                line,
                acc
                  .template slice<R - 1, 0>()
                  .template widen<W>()
                  .as_signed()
              );
              break;
            case State::neg:
              set_line(
                line,
                word_t(-1)
              );
              break;
            case State::zero:
              set_line(
                line,
                word_t(0)
              );
              break;
            case State::fix:
              set_line(
                line,
                acc
                  .template slice<R - 1, 0>()
                  .modularSucc()
                  .template widen<W>()
                  .as_signed()
              );
              break;
          }
        }

        // Compute sout's two's complement
        nsout = (~sout).modularAdd(carryin);

        // Handle the sign/significant bits special case
        auto capture = nsout
          .template slice<
            floatw<basefloat>::wf() + R - 1,
            floatw<basefloat>::wf()>()
          .or_reduce();
        if(capture) { // nsout R MSB looks like 01xx: capture
          captured_line = line;
          captured_nsout = nsout;
        }
        else if(state == State::neg) { // entering (neg) state: resolution
          // Using a flag instead of another find_candidate call to save
          // hardware
          resolution = true;
        }

#ifdef DEBUG
        // Debug info
        if((acc.or_reduce() || sout.or_reduce())
          && (acc.nand_reduce() || sout.nand_reduce())) // hide if acc.sout=-1
        {
          std::cerr
            << line
            << (valid ? "\t" : "+\t")
            << acc.toBinString() << " | "
            << sout.toBinString() << " "
            << carryin << " | "
            << nsout.toBinString() << " | "
            << (neg ? "<0" : "≥0") << ' ';
          switch(state) {
            case State::base: std::cerr << "(base)"; break;
            case State::neg:  std::cerr << "(neg) "; break;
            case State::zero: std::cerr << "(zero)"; break;
            case State::fix:  std::cerr << "(fix) "; break;
          }
          std::cerr
            << ' ' << (capture ? "*" : " ")
            << " >> " << (valid ? get_line(line) : 0)
            << '\n';
        }
#endif

        // Save the current candidate if sout's absolute value contains one
        find_candidate<basefloat>(
          (resolution ? true : neg), fs,
          (resolution ? captured_nsout : (neg ? nsout : sout)), m,
          (resolution ? captured_line : line), e
        );
        resolution = false;
      }

#ifdef DEBUG
      // Make subsequent calls of propagate() more clear
      std::cerr << '\n';
#endif

      // Handle special cases
      switch(fpclass) {
        case FPClass::Normal:
          break; // no special case: return result from the last candidate
        case FPClass::PositiveInfinity:
          return floatw<basefloat>::infinity();
        case FPClass::NegativeInfinity:
          return -floatw<basefloat>::infinity();
        case FPClass::NaN:
          return floatw<basefloat>::quiet_NaN();
      }

      // Compute the result from the last candidate

      // Result is too big: return ±infinity
      // Note: if signed_bit_t is narrower than wmax's width (we), we need to
      // change its width or wmax is interpreted as -1. Is this a bug in
      // intwrapper when comparing to (implicitly converted) native integer?
      if(e.template widen<(signed_bit_t::width < floatw<basefloat>::we()) ? floatw<basefloat>::we() : signed_bit_t::width>()
        > floatw<basefloat>::wmax()) {
        if(fs)
          return -floatw<basefloat>::infinity();
        else
          return floatw<basefloat>::infinity();
      }

      // Split mantissa and implicit bit
      uintw<1> normal;
      m.split(normal, fm);

      // Compute exponent field in (zero if result is subnormal)
      fe = floatw<basefloat>::exponent_t::mux(
        normal,
        // normalise_get_exponent<basefloat>(e),
        e
          // candidate's exponent can be larger or narrower than target format's
          // exponent, but we checked that it is valid (within range)
          .template setw<floatw<basefloat>::exponent_t::width>()
          .modularAdd(floatw<basefloat>::biasw())
          .as_unsigned(),
        0
      );

      return floatw<basefloat>(fs, fe, fm);
    }

    bool operator ==(const this_t& other) const {
      // Expect comparison functions use the same principle as the propagate()
      // function: they compute an accumulation over a sliding window of the
      // accumulator. They cannot directly compare lines has the representation
      // of a number is not unique.
      // The expect operands do not need to have their carries propagated.

      // Handle special cases
      if((fpclass == FPClass::NaN) || (other.fpclass == FPClass::NaN)) {
        return false; // unordered
      }
      else if(fpclass != other.fpclass) {
        return false;
      }
      else if((fpclass != FPClass::Normal) && (fpclass == other.fpclass)) {
        return true;
      }

      // Sliding window over the accumulators
      word_t accl(0), accr(0);

      compare:for(unsigned int line = 0; line < B; line++) {
#pragma HLS pipeline
        if(accl.template slice<R - 1, 0>() != accr.template slice<R - 1, 0>())
          return false;

        accl = (accl >> typename word_t::shift_t(R)).modularAdd(get_line(line));
        accr = (accr >> typename word_t::shift_t(R)).modularAdd(other.get_line(line));
      }

      // Note: we do NOT compare last bits above B×R (the overlap bits of the
      // last line) because they are not part of word. They are not taken in
      // account in propagation or normalisation.

      // Compare last line' R LSB
      return accl.template slice<R - 1, 0>() == accr.template slice<R - 1, 0>();
    }

    // iostream operators
    friend std::ostream& operator <<(std::ostream &out, const expect<B, W, R, Zero>& e) {
      int skipped = 0;
      for(int line = B - 1; line >= 0; line--) {
        if(!e.get_line(line))
          skipped++;
        else {
          if(skipped > 0) {
            out << std::setw(5) << (line + skipped) << "  ... skipped ...  " << (line + 1) << '\n';
            skipped = 0;
          }
          out <<
            std::setw(5) << line
            << " |" << e.get_line_overlap(line).toBinString()
            << ' ' << e.get_line_significant(line).toBinString()
            << "| " << e.get_line(line).as_signed()
            << " * 2**" << (line * int(R) - int(Zero))
            << '\n';
        }
      }
      if(skipped > 0) {
        out << std::setw(5) << (skipped - 1) << "  ... skipped ...  0" << '\n';
        skipped = 0;
      }

      // ruby expression on a single line
      for(int line = B - 1; line >= 0; line--) {
        if(e.get_line(line)) {
          out << e.get_line(line).as_signed() << " * 2**" << (line * int(R) - int(Zero)) << " + ";
        }
      }
      out << '\n';

      return out;
    }
};

#endif // __EXPECT_H
