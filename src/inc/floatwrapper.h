#ifndef __FLOATWRAPPER_H
#define __FLOATWRAPPER_H

#include <iostream>
#include "intwrapper.h"

/*
 * Simple float wrapper class
 * It adds a lot of OO methods for easier advanced float numbers manipulation
 * It proxies all operations to underlying float type
 */

// Vivado HLS hls_half.h header does not include numeric_limits definition for
// synthesis. This results in simulation working but synthesis failing because
// all fields evaluate to 0. What a great idea!
// We define the few we need here.
#ifdef __SYNTHESIS__
namespace std
{
  template<> struct numeric_limits<half>
  {
  public:
    static constexpr int digits = 11;
    static constexpr int min_exponent = -13;
    static constexpr int max_exponent = 16;
    static constexpr half infinity() {
      return half(0x7c00);
    }
    static constexpr half quiet_NaN() {
      return half(0x7fff);
    }
    static constexpr half signaling_NaN() {
      return half(0x7dff);
    }
  };
}
#endif // __SYNTHESIS__

template <typename basefloat>
class floatw {
  private:
    typedef floatw<basefloat> this_t;

    // compute base 2 logarithm (works on integer types), rounded up or down
    template <typename T>
    static constexpr T floorlog2(T x) {
      return (x == 1) ? 0 : 1 + floorlog2(x >> 1);
    }
    // this is also number of bits needed to represent at most x (excluded)
    // e.g if x = 16, how many bits are needed to represent 0..15
    template <typename T>
    static constexpr T ceillog2(T x) {
      return (x == 1) ? 0 : floorlog2(x - 1) + 1;
    }
    // this is also number of bits needed to represent x
    // also handles negative value (with +1 bit for sign bit)
    template <typename T>
    static constexpr T ceillog2p(T x) {
      return (x < 0) ? (ceillog2(-x + 1) + 1) : ceillog2(x + 1);
    }

    // For some reason, Vivado HLS' clang does not evaluate some recursive
    // constexpr functions at compile-time (but some are fine). Then SYNCHK is
    // not happy about the (runtime) recursive function not being supported.
    // This class forces a compile-time evaluation and solves the problem.
    template <typename T, T K>
    struct forceConstExpr {
      static constexpr T value = K;
    };

    basefloat m;

  public:
    // mantissa field size
    constexpr static int wf() {
      return std::numeric_limits<basefloat>::digits - 1;
    }

    // exponent field size
    constexpr static int we() {
      return forceConstExpr<int, ceillog2(
        std::numeric_limits<basefloat>::max_exponent
        - std::numeric_limits<basefloat>::min_exponent
      )>::value;
    }

    // exponent bias
    constexpr static int bias() {
      return (1 << (we() - 1)) - 1;
    }

    // minimal representable power of 2
    constexpr static int wmin() {
      return - bias() + 1 - wf();
    }

    // maximal representable power of 2
    constexpr static int wmax() {
      return bias();
    }

    // infinity and NaNs
    static this_t infinity() {
      return this_t(std::numeric_limits<basefloat>::infinity());
    }
    static this_t quiet_NaN() {
      return this_t(std::numeric_limits<basefloat>::quiet_NaN());
    }
    static this_t signaling_NaN() {
      return this_t(std::numeric_limits<basefloat>::signaling_NaN());
    }

    // same with intw returns
    constexpr static intw<ceillog2p(wf()) + 1, true> wfw() {
      // width +1 for sign (0)
      return intw<ceillog2p(wf()) + 1, true>(wf());
    }
    constexpr static intw<ceillog2p(we()) + 1, true> wew() {
      // width +1 for sign (0)
      return intw<ceillog2p(we()) + 1, true>(we());
    }
    constexpr static intw<we(), true> biasw() {
      return intw<we(), true>(bias());
    }
    constexpr static intw<ceillog2p(wmin()), true> wminw() {
      return intw<ceillog2p(wmin()), true>(wmin());
    }
    constexpr static intw<we(), true> wmaxw() {
      return intw<we(), true>(wmax());
    }

    // related integer types
    typedef intw<1, false> sign_t;
    typedef intw<wf(), false> mantissa_t;
    typedef intw<1 + wf(), false> explicit_mantissa_t;
    typedef intw<2 + wf(), true> signed_explicit_mantissa_t;
    typedef intw<we(), false> exponent_t;
    typedef intw<we(), true> real_exponent_t;
    typedef intw<1 + we() + wf(), false> mem_t;

    // constructors
    floatw()
    {}
    floatw(basefloat _m)
      : m(_m)
    {}
    floatw(intw<1 + we() + wf(), false> _m)
      : m(_m.template convert<basefloat>())
    {}
    floatw(const sign_t& sign, const exponent_t& exponent, const mantissa_t& mantissa)
      : m(sign.concat(exponent).concat(mantissa).template convert<basefloat>())
    {}
    // TODO: constructors for floating point literals (1.f, 0.f). Would be handy
    // to stop having to force the FP type to avoid non-sense integer
    // conversion. It seems difficult to do wihout conflicting with the
    // floatw(basefloat) constructor (cannot SFINAE a class template)

    // sign accessor
    sign_t sign() const {
      return mem().template get<we() + wf()>();
    }

    // exponent accessor (biased)
    exponent_t exponent() const {
      return mem().template slice<we() + wf() - 1, wf()>();
    }

    // exponent, corrected for subnormals (biased)
    exponent_t corrected_exponent() const {
      return exponent_t::mux(
        bool(exponent().or_reduce()),
        exponent(),
        1
      );
    }

    // exponent accessor (real)
    real_exponent_t real_exponent() const {
      return corrected_exponent().as_signed().modularSub(biasw());
    }

    // mantissa accessor
    mantissa_t mantissa() const {
      return mem().template slice<wf() - 1, 0>();
    }

    // explicit mantissa (0m) for subnormal, 1m for normal) getter
    explicit_mantissa_t explicit_mantissa() const {
      return intw<1, false>::mux(
        bool(exponent().or_reduce()),
        1,
        0
      ).concat(mantissa());
    }

    // explicit mantissa * sign getter
    signed_explicit_mantissa_t signed_explicit_mantissa() const {
      return signed_explicit_mantissa_t::mux(
        bool(sign()),
        (~(uintw<1>(0).concat(explicit_mantissa()))).modularSucc().as_signed(), // negation
        uintw<1>(0).concat(explicit_mantissa()).as_signed()
      );
    }

    // "memory layout" accessor
    mem_t mem() const {
      return mem_t::convert(m);
    }

    // native float accessor
    basefloat& unravel() const {
      return m;
    }

    // proxy operators
    this_t operator +() const {
      return *this;
    }
    this_t operator -() const {
      return this_t(basefloat(-m));
    }
    this_t operator +(const this_t& rhs) const {
      return this_t(basefloat(m + rhs.m));
    }
    this_t operator -(const this_t& rhs) const {
      return this_t(basefloat(m - rhs.m));
    }
    this_t operator *(const this_t& rhs) const {
      return this_t(basefloat(m * rhs.m));
    }
    this_t operator /(const this_t& rhs) const {
      return this_t(basefloat(m / rhs.m));
    }

    this_t& operator =(const this_t& other) {
      m = other.m;
      return *this;
    }
    this_t& operator +=(const this_t& rhs) {
      m += rhs.m;
      return *this;
    }
    this_t& operator -=(const this_t& rhs) {
      m -= rhs.m;
      return *this;
    }
    this_t& operator *=(const this_t& rhs) {
      m *= rhs.m;
      return *this;
    }
    this_t& operator /=(const this_t& rhs) {
      m /= rhs.m;
      return *this;
    }

    bool operator ==(const this_t& other) const {
      if(isnan() || other.isnan()) return false;
      return m == other.m;
    }
    bool operator !=(const this_t& other) const {
      if(isnan() || other.isnan()) return true;
      return m != other.m;
    }
    bool operator <(const this_t& other) const {
      if(isnan() || other.isnan()) return false;
      return m < other.m;
    }
    bool operator >(const this_t& other) const {
      if(isnan() || other.isnan()) return false;
      return m > other.m;
    }
    bool operator <=(const this_t& other) const {
      if(isnan() || other.isnan()) return false;
      return m <= other.m;
    }
    bool operator >=(const this_t& other) const {
      if(isnan() || other.isnan()) return false;
      return m >= other.m;
    }

    // Compute difference as "ULP". Useful to check if diff <= 1ULP
    uintw<mem_t::width + 1> diff(const floatw<basefloat>& rhs) const {
      return mem().dist(rhs.mem());
    }

    bool isnan() const {
      return exponent().and_reduce() && mantissa().or_reduce();
    }
    bool isinf() const {
      return exponent().and_reduce() && mantissa().nor_reduce();
    }
    bool isfinite() const {
      return !isnan() && !isinf();
    }
    bool isnormal() const {
      return exponent().nand_reduce() && exponent().or_reduce();
    }
    // Non standard but handy
    bool is_subnormal() const { // Note: issubnormal is a macro in math.h…
      return exponent().nor_reduce();
    }
    bool iszero() const {
      return exponent().nor_reduce() && mantissa().nor_reduce();
    }

    // iostream operators
    friend std::ostream& operator<<(std::ostream &out, const floatw<basefloat>& f) {
      out
        << f.m
        << "["
        << f.sign() << ' '
        << f.exponent().toHexString() << ' '
        << f.mantissa().toHexString()
        << " = ";
      if(f.sign())
        out << '-';
      if(f.isnan())
        out << "NaN";
      else if(f.isinf())
        out << "inf";
      else
        out
          << f.explicit_mantissa()
          << " * 2**-" << wf() <<
          " * 2**" << f.real_exponent();
      out << ']';

      return out;
    }
    friend std::istream& operator<<(std::istream &out, floatw<basefloat>& f) {
      return out >> f.m;
    }
};

#endif // __FLOATWRAPPER_H
