#ifndef __INTWRAPPER_H
#define __INTWRAPPER_H

#include <ap_int.h>
#include <type_traits>
#include <algorithm>
#include <iostream>

/*
 * Simple arbitrary size int wrapper class
 * It adds static checks to ap_int types
 * It proxies all operations to underlying ap_int type
 *
 * This wrappers allow W = 0, which can be handy, and implement some operations
 * as noop if W = 0: split.
 */

template <unsigned int W, bool is_signed>
class intw {
  private:
    template <unsigned int W2, bool is_signed2>
    struct baseint {};

    template <unsigned int W2>
    struct baseint<W2, true> {
      typedef ap_int<W2> type;
    };

    template <unsigned int W2>
    struct baseint<W2, false> {
      typedef ap_uint<W2> type;
    };

    // compute base 2 logarithm (works on integer types), rounded up or down
    template <typename T>
    static constexpr T floorlog2(T x) {
      return (x == 1) ? 0 : 1 + floorlog2(x >> 1);
    }
    // this is also number of bits needed to represent at most x (excluded)
    // e.g if x = 16, how many bits are needed to represent 0..15
    template <typename T>
    static constexpr T ceillog2(T x) {
      return (x == 1) ? 0 : floorlog2(x - 1) + 1;
    }

    // result width of left shift of W size lhs with W2 size rhs
    template <unsigned int W2>
    static constexpr unsigned int shlwidth() {
      return W + (1 << W2) - 1;
    }
    // result width of sum of W size lhs with W2 size rhs
    // TODO only if both sign are the same (ug902: W+W2+1 or +2 iff the wider is unsigned and the narrower is signed)
    template <unsigned int W2>
    static constexpr unsigned int addwidth() {
      // unfortunately std::max is not constexpr in VHLS
      return ((W > W2) ? W : W2) + 1;
    }
    // result width of product of W size lhs with W2 size rhs
    // do not duplicate sign bit if the product is signed
    template <unsigned int W2>
    static constexpr unsigned int mulwidth() {
      return W + W2;
    }
    // result width of inverse (signed) / absolute value (unsigned)
    static constexpr unsigned int invwidth() {
      return is_signed ? W + 1 : W;
    }

    typename baseint<W, is_signed>::type m;
    // Classes instantiated for different <W, is_signed> need to access to m
    // member
    template <unsigned int W2, bool is_signed2>
    friend class intw;

  public:
    typedef intw<ceillog2(W + 1), false> shift_t;
    typedef intw<W, true> as_signed_t;
    typedef intw<W, false> as_unsigned_t;

    // constructors
    intw()
      : m(0)
    {}
    // copy constructor
    intw(typename baseint<W, is_signed>::type _m)
      : m(_m)
    {}
    // constructor from integral types
    // Note: this constructor is not explicit to allow the use of int literals
    // without explicitly write intw<>()
    intw(int _m)
      : m(_m)
    {}
    // Note: these constructors must be explicit to avoid (among others)
    // ambiguity on functions accepting intw (which could be implicitly
    // converted to integral types). E.g., floatw constructor on double:
    // floatw<double>(0.)
    // > error: conversion from 'double' to 'intw<1 + we() + wf(), false>' is
    // > ambiguous
    explicit intw(unsigned int _m)
      : m(_m)
    {}
    explicit intw(long int _m)
      : m(_m)
    {}
    explicit intw(unsigned long int _m)
      : m(_m)
    {}

    // true is value is non-zero
    // https://stackoverflow.com/a/16615725
    explicit operator bool() const {
      return or_reduce() == intw<1, false>(1);
    }

    // ap_int accessor
    // TODO baseint<W, is_signed>() (see copy/move constructors)
    const typename baseint<W, is_signed>::type& unravel() const {
      return m;
    }
    typename baseint<W, is_signed>::type& unravel() {
      return m;
    }

    // allows any type to be bitwise examined
    template <typename T>
    static typename std::enable_if<sizeof(T) * 8 == W, intw<W, is_signed>>::type convert(const T& v) {
      return *reinterpret_cast<const intw<W, is_signed>*>(&v);
    }
    template <typename T>
    typename std::enable_if<sizeof(T) * 8 == W, T>::type convert() const {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
      return *reinterpret_cast<const T*>(&m);
#pragma GCC diagnostic pop
    }

    as_unsigned_t as_unsigned() const {
      return typename baseint<W, false>::type(m);
    }
    as_signed_t as_signed() const {
      return typename baseint<W, true>::type(m);
    }

    static intw<W, is_signed> mux(bool c, intw<W, is_signed> yes, intw<W, is_signed> no) {
      return intw<W, is_signed>(c ? yes.m : no.m);
    }

    template <bool is_signed2>
    static intw<W, is_signed> mux(intw<1, is_signed2> c, intw<W, is_signed> yes, intw<W, is_signed> no) {
      return mux(bool(c), yes, no);
    }

    static const bool signness = is_signed;
    static const int width = W;
    constexpr unsigned int length() const {
      return W;
    }

    template <unsigned int W2>
    typename std::enable_if<W <= W2, intw<W2, is_signed>>::type widen() const {
      return intw<W2, is_signed>(typename baseint<W2, is_signed>::type(m));
    }

    template <unsigned int high, unsigned int low>
    typename std::enable_if<high >= low and high < W, intw<high - low + 1, false>>::type slice() const {
      return intw<high - low + 1, false>(typename baseint<high - low + 1, false>::type(m.range(high, low)));
    }

    // Either widen or slice
    // Useful in parametrized code (when some type may be narrower or larger
    // than another type) and if you know what you do
    // (Allow slice to return a signed type)
    template <unsigned int W2>
    typename std::enable_if<W <= W2, intw<W2, is_signed>>::type setw() const {
      return widen<W2>();
    }
    template <unsigned int W2>
    typename std::enable_if<(W > W2) && is_signed, intw<W2, is_signed>>::type setw() const {
      return slice<W2 - 1, 0>().as_signed();
    }
    template <unsigned int W2>
    typename std::enable_if<(W > W2) && !is_signed, intw<W2, is_signed>>::type setw() const {
      return slice<W2 - 1, 0>();
    }
    // TODO is is_signed / !is_signed needed

    template <unsigned int idx>
    typename std::enable_if<idx < W, intw<1, false>>::type get() const {
      return intw<1, false>(m[idx]);
    }
    intw<1, false> sign() const {
      return intw<1, false>::mux(m.sign(), 1, 0);
    }
    template <unsigned int idx>
    typename std::enable_if<idx < W, bool>::type test() const {
      return m[idx] == 1;
    }
    template <unsigned int idx>
    typename std::enable_if<idx < W, intw<W, is_signed>&>::type set(const intw<1, false>& v) {
      m.set(idx, v.m);
      return *this;
    }
    template <unsigned int idx>
    typename std::enable_if<idx < W, intw<W, is_signed>&>::type set() {
      m.set(idx);
      return *this;
    }
    template <unsigned int idx>
    typename std::enable_if<idx < W, intw<W, is_signed>&>::type clear() {
      m.clear(idx);
      return *this;
    }
    template <unsigned int idx>
    typename std::enable_if<idx < W, intw<W, is_signed>&>::type invert() {
      m.invert(idx);
      return *this;
    }

    template <unsigned int W2>
    typename std::enable_if<W2 <= W, intw<W, is_signed>>::type modularAdd(const intw<W2, is_signed>& rhs) const {
      return intw<W, is_signed>(typename baseint<W, is_signed>::type(m + rhs.m));
    }
    template <unsigned int W2>
    typename std::enable_if<W2 <= W, intw<W, is_signed>>::type modularSub(const intw<W2, is_signed>& rhs) const {
      return intw<W, is_signed>(typename baseint<W, is_signed>::type(m - rhs.m));
    }

    // +1 and -1
    intw<W + 1, is_signed> pred() const {
      return intw<W + 1, is_signed>(typename baseint<W + 1, is_signed>::type(m - 1));
    }
    intw<W + 1, is_signed> succ() const {
      return intw<W + 1, is_signed>(typename baseint<W + 1, is_signed>::type(m + 1));
    }

    // modular +1 and -1
    intw<W, is_signed> modularPred() const {
      return intw<W, is_signed>(typename baseint<W, is_signed>::type(m - 1));
    }
    intw<W, is_signed> modularSucc() const {
      return intw<W, is_signed>(typename baseint<W, is_signed>::type(m + 1));
    }

    // "more complex" proxy operators
    // concatenation does not have arithmetic meaning: we allow concatenation
    // of all possible signed/unsigned pairs but they always result in a
    // unsigned word
    template <unsigned int W2, bool is_signed2>
    intw<W + W2, false> operator ,(const intw<W2, is_signed2>& rhs) const {
      return concat(rhs);
    }
    template <unsigned int W2, bool is_signed2>
    intw<W + W2, false> concat(const intw<W2, is_signed2>& rhs) const {
      return intw<W + W2, false>(typename baseint<W + W2, false>::type(m.concat(rhs.m)));
    }
    // alternative to lvalue (operator,) : (a, b) = c
    template <unsigned int W2, bool is_signed2, unsigned int W3, bool is_signed3>
    typename std::enable_if<(W == W2 + W3) && (W2 > 0) && (W3 > 0), void>::type split(intw<W2, is_signed2>& left, intw<W3, is_signed3>& right) const {
      (left.m, right.m) = m;
    }
    // zero-bit versions
    template <unsigned int W2, bool is_signed2, unsigned int W3, bool is_signed3>
    typename std::enable_if<(W == W2 + W3) && (W2 == 0), void>::type split(intw<W2, is_signed2>& left, intw<W3, is_signed3>& right) const {
      right.m = m;
    }
    template <unsigned int W2, bool is_signed2, unsigned int W3, bool is_signed3>
    typename std::enable_if<(W == W2 + W3) && (W3 == 0), void>::type split(intw<W2, is_signed2>& left, intw<W3, is_signed3>& right) const {
      left.m = m;
    }

    intw<W, is_signed> operator =(const intw<W, is_signed>& rhs) {
      m = rhs.m;
      return *this;
    }

    // Note: for shift and rotate operators, static checks are not perfect if W
    // is not a power of 2, but rhs's size cannot be wider than necessary
    // Note: for right shift, we allow rhs type to be narrower than shift_t
    // because that would generate simpler hardware (not the full barrel
    // shifter). Note that for left shift, rhs width can be arbitrary large.
    template <unsigned int W2>
    typename std::enable_if<W2 <= shift_t::width, intw<W, is_signed>>::type operator >>(const intw<W2, false>& rhs) const {
      return intw<W, is_signed>(typename baseint<W, is_signed>::type(m >> rhs.m));
    }
    template <unsigned int W2>
    intw<shlwidth<W2>(), is_signed> operator <<(const intw<W2, false>& rhs) const {
      return intw<shlwidth<W2>(), is_signed>(
        typename baseint<shlwidth<W2>(), is_signed>::type(m) << rhs.m
      );
    }

    // TODO same as operator >>
    intw<W, is_signed> rrotate(const shift_t& rhs) const {
      return intw<W, is_signed>(m.rrotate(rhs.m));
    }
    intw<W, is_signed> lrotate(const shift_t& rhs) const {
      return intw<W, is_signed>(m.lrotate(rhs.m));
    }

    // shift and insert a word but do not change the size
    template <unsigned int W2, bool is_signed2>
    typename std::enable_if<W2 <= W, intw<W, is_signed>>::type rinsert(const intw<W2, is_signed2>& right) const {
      return slice<W - W2 - 1, 0>().concat(right);
    }
    template <unsigned int W2, bool is_signed2>
    typename std::enable_if<W2 <= W, intw<W, is_signed>>::type linsert(const intw<W2, is_signed2>& left) const {
      return left.concat(slice<W - 1, W2>());
    }

    // Leading one position: return position of leading 1 in the word
    // return -1 if no 1 is found
    shift_t lop() const {
      for(int i = W - 1; i >= 0; i--) {
#pragma HLS UNROLL
        if(m.test(i)) return i;
      }
      return -1;
    }

    // TODO: could be implemented with std::abs(), like others?
    intw<invwidth(), false> abs() const {
      return intw<invwidth(), false>(
        (m < 0)
          ? intw<invwidth(), false>(-m)
          : intw<invwidth(), false>(m)
      );
    }

    intw<1, false> and_reduce() const {
      return intw<1, false>(m.and_reduce() ? 1 : 0);
    }
    intw<1, false> or_reduce() const {
      return intw<1, false>(m.or_reduce() ? 1 : 0);
    }
    intw<1, false> xor_reduce() const {
      return intw<1, false>(m.xor_reduce() ? 1 : 0);
    }
    intw<1, false> nand_reduce() const {
      return intw<1, false>(m.nand_reduce() ? 1 : 0);
    }
    intw<1, false> nor_reduce() const {
      return intw<1, false>(m.nor_reduce() ? 1 : 0);
    }
    intw<1, false> xnor_reduce() const {
      return intw<1, false>(m.xnor_reduce() ? 1 : 0);
    }

    // simple proxy operators
    intw<W, is_signed> operator +() const {
      return *this;
    }
    intw<invwidth(), true> operator -() const {
      return intw<invwidth(), true>(-m);
    }
    intw<W, is_signed> operator ~() const {
      return intw<W, is_signed>(~m);
    }
    void b_not() {
      m.b_not();
    }
    intw<W, is_signed> reverse() const {
      return intw<W, is_signed>(m.reverse);
    }
    intw<W, is_signed> operator |(const intw<W, is_signed>& rhs) const {
      return intw<W, is_signed>(m | rhs.m);
    }
    intw<W, is_signed> operator &(const intw<W, is_signed>& rhs) const {
      return intw<W, is_signed>(m & rhs.m);
    }
    intw<W, is_signed> operator ^(const intw<W, is_signed>& rhs) const {
      return intw<W, is_signed>(m ^ rhs.m);
    }
    template<unsigned int W2>
    intw<addwidth<W2>(), is_signed> operator +(const intw<W2, is_signed>& rhs) const {
      return intw<addwidth<W2>(), is_signed>(m + rhs.m);
    }
    template<unsigned int W2>
    intw<addwidth<W2>(), is_signed> operator -(const intw<W2, is_signed>& rhs) const {
      return intw<addwidth<W2>(), is_signed>(typename baseint<addwidth<W2>(), is_signed>::type(m - rhs.m));
    }
    // distance, i.e. absolute value of difference (with only 1 extra bit, not 2)
    template<unsigned int W2>
    intw<addwidth<W2>(), is_signed> dist(const intw<W2, is_signed>& rhs) const {
      return intw<addwidth<W2>(), is_signed>(typename baseint<addwidth<W2>(), is_signed>::type(std::abs(m - rhs.m)));
    }
    template<unsigned int W2>
    intw<mulwidth<W2>(), is_signed> operator *(const intw<W2, is_signed>& rhs) const {
      return intw<mulwidth<W2>(), is_signed>(typename baseint<mulwidth<W2>(), is_signed>::type(m * rhs.m));
    }
    /* intw<W, is_signed> operator /(const intw<W, is_signed>& rhs) const {
      return intw<W, is_signed>(m + rhs.m);
    } */
    /* TODO operator % */

    bool operator ==(const intw<W, is_signed>& other) const {
      return m == other.m;
    }
    bool operator !=(const intw<W, is_signed>& other) const {
      return m != other.m;
    }
    bool operator <(const intw<W, is_signed>& other) const {
      return m < other.m;
    }
    bool operator >(const intw<W, is_signed>& other) const {
      return m > other.m;
    }
    bool operator <=(const intw<W, is_signed>& other) const {
      return m <= other.m;
    }
    bool operator >=(const intw<W, is_signed>& other) const {
      return m >= other.m;
    }

    // iostream operators
    friend std::ostream& operator <<(std::ostream &out, const intw<W, is_signed>& i) {
      return out << i.m;
    }
    friend std::istream& operator >>(std::istream &out, intw<W, is_signed>& i) {
      return out >> i.m;
    }
    // there is no std::bin, so we expose a method to do it
    std::string toBinString() const {
      std::string r;
      r.resize(W);
      for(unsigned int i = 0; i < W; i++)
        r[W - i - 1] = m.test(i) ? '1' : '0';
      return r;
    }
    // ap_int does not honor std::hex, so we expose a method to do it (two's complement, with zero fill)
    std::string toHexString() const {
      std::string r;
      const int s = (W + 3) / 4;
      r.resize(s);
      auto c = as_unsigned();
      for(unsigned int i = 0; i < s; i++) {
        auto h = c.template slice<3, 0>().unravel();
        if(h < 10)
          r[s - i - 1] = h + '0';
        else
          r[s - i - 1] = h - 10 + 'a';
        c = c >> intw<3, false>(4);
      }
      return r;
    }
};

// Shortcuts
template <unsigned int W>
using sintw = intw<W, true>;
template <unsigned int W>
using uintw = intw<W, false>;

#endif // __INTWRAPPER_H
