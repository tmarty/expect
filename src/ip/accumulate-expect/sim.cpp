#include <random>
#include "hw.h"

// Note: without gtest and mpfr, we do a simple test with only one accumulation
// It would be nice to reuse our tests…

int main() {
  std::random_device rd;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist_s(0, 1);
  std::uniform_int_distribution<long int> dist_e(
    0, (1L << floatw<expect_basefloat>::we()) - 1 - 1 // -1 avoids NaN and inf
  );
  std::uniform_int_distribution<long int> dist_m(
    0, (1L << floatw<expect_basefloat>::wf()) - 1
  );

  bool failed = false;
  expect_t l, r, ref;
  floatw<expect_basefloat> f0, f1;

  for(int i = 0; i < 10; i++) {
    ref.reset();
    l.reset();
    r.reset();

    f0 = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );

    l.accumulate(f0);
    ref.accumulate(f0);

    f1 = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );

    r.accumulate(f1);
    ref.accumulate(f1);

    hw_toplevel(l, r);

    if(l == ref)
      std::cerr << "test passed: " << f0 << " + " << f1 << '\n';
    else {
      std::cerr << "test failed: " << f0 << " + " << f1 << '\n';
      failed = true;
    }
  }

  return failed ? 1 : 0;
}
