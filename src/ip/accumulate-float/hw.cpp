#include "hw.h"

void hw_toplevel(expect_t& e, floatw<expect_basefloat> f) {
  e.accumulate(f);
}
