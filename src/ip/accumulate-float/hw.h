#ifndef __HW_H
#define __HW_H

#include <hls_half.h>
#include "expect.h"

typedef expect<expect_B, expect_W, expect_R, expect_Zero> expect_t;

void hw_toplevel(expect_t& e, floatw<expect_basefloat> f);

#endif // __HW_H
