#include <random>
#include "hw.h"

// Note: without gtest and mpfr, we do a simple test with only one accumulation
// It would be nice to reuse our tests…

int main() {
  std::random_device rd;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist_s(0, 1);
  std::uniform_int_distribution<long int> dist_e(
    0, (1L << floatw<expect_basefloat>::we()) - 1
  );
  std::uniform_int_distribution<long int> dist_m(
    0, (1L << floatw<expect_basefloat>::wf()) - 1
  );

  bool failed = false;
  expect_t e;
  floatw<expect_basefloat> ref, res;

  for(int i = 0; i < 10; i++) {
    ref = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );

    e.reset();

    hw_toplevel(e, ref);

    res = e.propagate<expect_basefloat>();

    if((ref.isnan() && res.isnan()) || (ref == res))
      std::cerr << "test passed: " << ref << '\n';
    else {
      std::cerr << "test failed:\n\t" << ref << "\n\t" << res << '\n';
      failed = true;
    }
  }

  return failed ? 1 : 0;
}
