#include "hw.h"

void hw_toplevel(expect_t& l, expect_t& r, bool *b) {
  *b = l == r;
}
