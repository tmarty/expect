#include <random>
#include "hw.h"

// Note: without gtest and mpfr, we do a simple test with only one accumulation
// It would be nice to reuse our tests…

int main() {
  std::random_device rd;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist_s(0, 1);
  std::uniform_int_distribution<long int> dist_e(
    0, (1L << floatw<expect_basefloat>::we()) - 1
  );
  std::uniform_int_distribution<long int> dist_m(
    0, (1L << floatw<expect_basefloat>::wf()) - 1
  );

  bool failed = false;
  expect_t l, r;
  floatw<expect_basefloat> ref, ref2;
  bool cmp;

  for(int i = 0; i < 10; i++) {
    ref = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );

    l.reset();
    r.reset();

    l.accumulate(ref);
    r.accumulate(ref);

    hw_toplevel(l, r, &cmp);

    if((ref.isnan() || cmp) && // !ref.isnan() -> cmp
      (!ref.isnan() || !cmp))  // ref.isnan() -> !cmp
      std::cerr << "test passed: " << ref << '\n';
    else {
      std::cerr << "test failed: " << ref << '\n';
      failed = true;
    }

    ref2 = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );
    l.accumulate(ref2);

    hw_toplevel(l, r, &cmp);

    if(!cmp)
      std::cerr << "test passed: " << ref << " + " << ref2 << '\n';
    else {
      std::cerr << "test failed: " << ref << " + " << ref2 << '\n';
      failed = true;
    }
  }

  return failed ? 1 : 0;
}
