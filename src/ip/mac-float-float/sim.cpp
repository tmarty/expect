#include <random>
#include "hw.h"

// Note: without gtest and mpfr, we do a simple test with only one accumulation
// It would be nice to reuse our tests…

int main() {
  std::random_device rd;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist_s(0, 1);
  std::uniform_int_distribution<long int> dist_e(
    0, (1L << floatw<expect_basefloat>::we()) - 1
  );
  std::uniform_int_distribution<long int> dist_m(
    0, (1L << floatw<expect_basefloat>::wf()) - 1
  );

  bool failed = false;
  expect_t e, eref;
  floatw<expect_basefloat> l, r, ref, res;

  for(int i = 0; i < 10; i++) {
    l = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );
    r = floatw<expect_basefloat>(
      floatw<expect_basefloat>::sign_t(dist_s(gen)),
      floatw<expect_basefloat>::exponent_t(dist_e(gen)),
      floatw<expect_basefloat>::mantissa_t(dist_m(gen))
    );

    eref.reset();
    e.reset();

    hw_toplevel(e, l, r);

    // Note: we can't rely on Xilinx FP cores as they flush subnormals to 0
    eref.mac(l, r);

    res = e.propagate<expect_basefloat>();

    ref = eref.propagate<expect_basefloat>();

    if((ref.isnan() && res.isnan()) || (ref.diff(res) <= 1))
      std::cerr << "test passed: " << ref << " = " << l << " * " << r << '\n';
    else {
      std::cerr << "test failed:\n\t"
        << ref << " = " << l << " * " << r << "\n\t"
        << res << '\n';
      failed = true;
    }
  }

  return failed ? 1 : 0;
}
