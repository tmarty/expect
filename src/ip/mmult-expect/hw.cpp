#include "hw.h"

void hw_mat_copy(floatw<expect_basefloat> mat[N][N], floatw<expect_basefloat> hw_mat[N][N]) {
  hw_mat_copy:for(int i = 0; i < N; i++) {
    for(int j = 0; j < N; j++) {
#pragma HLS pipeline
      hw_mat[i][j] = mat[i][j];
    }
  }
}

void hw_mat_copy_transpose(floatw<expect_basefloat> mat[N][N], floatw<expect_basefloat> hw_mat[N][N]) {
  hw_mat_copy_transpose:for(int i = 0; i < N; i++) {
    for(int j = 0; j < N; j++) {
#pragma HLS pipeline
      hw_mat[j][i] = mat[i][j];
    }
  }
}

void hw_dotproduct(floatw<expect_basefloat> l[N], floatw<expect_basefloat> r[N], floatw<expect_basefloat>& p) {
  expect_t e;
  hw_dotproduct:for(int n = 0; n < N; n++) {
#pragma HLS pipeline
    e.mac(l[n], r[n]);
  }
  p = e.propagate<expect_basefloat>();
}

void hw_mmult(floatw<expect_basefloat> hw_l[N][N], floatw<expect_basefloat> hw_r[N][N], floatw<expect_basefloat> hw_p[N][N]) {
  hw_mmult:for(int i = 0; i < N; i++) {
    for(int j = 0; j < N; j++) {
      hw_dotproduct(hw_l[i], hw_r[j], hw_p[i][j]);
    }
  }
}

void hw_toplevel(floatw<expect_basefloat> l[N][N], floatw<expect_basefloat> r[N][N], floatw<expect_basefloat> p[N][N]) {
#ifdef __SYNTHESIS__
  floatw<expect_basefloat> hw_l[N][N];
  floatw<expect_basefloat> hw_r[N][N];
  floatw<expect_basefloat> hw_p[N][N];
#else // __SYNTHESIS__
  // If N is large, we need to allocate on heap
  auto hw_l = new floatw<expect_basefloat>[N][N];
  auto hw_r = new floatw<expect_basefloat>[N][N];
  auto hw_p = new floatw<expect_basefloat>[N][N];
#endif // __SYNTHESIS__

  // FIXME WARNING: [HLS 200-786] Detected dataflow-on-top in function  'hw_toplevel' (hw.cpp:38)  with default interface mode 'ap_ctrl_hs'. Overlapped execution of successive kernel calls will not happen unless interface mode 'ap_ctrl_chain' is used (or 'ap_ctrl_none' for a purely data-driven design).
#pragma HLS dataflow

  hw_mat_copy(l, hw_l);
  hw_mat_copy_transpose(r, hw_r);
  hw_mmult(hw_l, hw_r, hw_p);
  hw_mat_copy(hw_p, p);

#ifndef __SYNTHESIS__
  delete[] hw_l;
  delete[] hw_r;
  delete[] hw_p;
#endif // __SYNTHESIS__
}
