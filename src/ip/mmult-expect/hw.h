#ifndef __HW_H
#define __HW_H

#include <hls_half.h>
#include "expect.h"

// FIXME N has constraints from expect parameters and basefloat
#define N 128

typedef expect<expect_B, expect_W, expect_R, expect_Zero> expect_t;

void hw_toplevel(floatw<expect_basefloat> l[N][N], floatw<expect_basefloat> r[N][N], floatw<expect_basefloat> p[N][N]);

#endif // __HW_H
