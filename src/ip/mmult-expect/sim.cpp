#include <random>
#include "hw.h"

// Note: without gtest and mpfr, we do a simple test with only one accumulation
// It would be nice to reuse our tests…

int main() {
  std::random_device rd;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist_s(0, 1);
  // Note: this upper limit avoids NaN and inf inputs and testing only infinite
  // results. It is choosen such that the square of the maximum exponent is
  // considered infinite, but the square of the next-to-maximum is fintie.
  // For we=8, it draws biased exponent from 0 to 191, i.e. exponent from -126
  // to 64. 2^64^2 = 2^128 ; 2^63^2 = 2^126 (the maximum finite exponent is
  // 2^127)
  std::uniform_int_distribution<long int> dist_e(
    0, (3L << (floatw<expect_basefloat>::we() - 2)) - 1
  );
  std::uniform_int_distribution<long int> dist_m(
    0, (1L << floatw<expect_basefloat>::wf()) - 1
  );

  int failed_outputs;
  bool failed = false;
  expect_t e;
  auto l = new floatw<expect_basefloat>[N][N];
  auto r = new floatw<expect_basefloat>[N][N];
  auto res = new floatw<expect_basefloat>[N][N];
  auto ref = new floatw<expect_basefloat>[N][N];

  // This test takes time, run only twice
  for(int t = 0; t < 2; t++) {
    for(int i = 0; i < N; i++) {
      for(int j = 0; j < N; j++) {
        l[i][j] = floatw<expect_basefloat>(
          floatw<expect_basefloat>::sign_t(dist_s(gen)),
          floatw<expect_basefloat>::exponent_t(dist_e(gen)),
          floatw<expect_basefloat>::mantissa_t(dist_m(gen))
        );
        r[i][j] = floatw<expect_basefloat>(
          floatw<expect_basefloat>::sign_t(dist_s(gen)),
          floatw<expect_basefloat>::exponent_t(dist_e(gen)),
          floatw<expect_basefloat>::mantissa_t(dist_m(gen))
        );
      }
    }

    hw_toplevel(l, r, res);

    for(int i = 0; i < N; i++) {
      for(int j = 0; j < N; j++) {
        e.reset();
        for(int k = 0; k < N; k++) {
          e.mac(l[i][k], r[k][j]);
        }
        ref[i][j] = e.propagate<expect_basefloat>();
      }
    }

    failed_outputs = 0;
    for(int i = 0; i < N; i++) {
      for(int j = 0; j < N; j++) {
        if(!((ref[i][j].isnan() && res[i][j].isnan()) || (ref[i][j] == res[i][j])))
          failed_outputs++;
      }
    }
    if(failed_outputs > 0) {
      std::cerr << "test failed: " << failed_outputs << " / " << (N * N) << " outputs wrong\n";
      failed = true;
    }
    else
      std::cerr << "test passed\n";
  }

  delete[] l;
  delete[] r;
  delete[] res;
  delete[] ref;

  return failed ? 1 : 0;
}
