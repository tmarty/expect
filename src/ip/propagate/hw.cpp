#include "hw.h"

void hw_toplevel(expect_t& e, floatw<expect_basefloat>& f) {
  f = e.propagate<expect_basefloat>();
}
