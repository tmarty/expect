#include <gtest/gtest.h>
#include <iostream>
#include <half.hpp>
#include <mpfr.h>
#include <random>
#include "expect.h"

template <typename T>
class ExpectTest : public ::testing::Test {
};

template <typename E, typename F>
struct param {
  typedef E expect;
  typedef F basefloat;
};

// Shortcuts to make the code much more legible.
// C++ typename / template is a mess.
#define E typename TypeParam::expect
#define F typename TypeParam::basefloat
#define FW(x) floatw<F>(F(x))
#define P template propagate<F>

// concatMapStringsSep "\n" ({ B, R, W, Zero, basefloat }: ", param<expect<${toString B}, ${toString W}, ${toString R}, ${toString Zero}>, ${basefloat}>") ips.configs
using ExpectTypes = ::testing::Types<
    param<expect<565, 60, 1, 298>, float>
  , param<expect<283, 60, 2, 298>, float>
  , param<expect<284, 60, 2, 299>, float> // shift zero to test alignments
  , param<expect<142, 60, 4, 298>, float>
  , param<expect<71,  60, 8, 298>, float>

  , param<expect<91, 34, 1, 48>, half_float::half>
  , param<expect<46, 34, 2, 48>, half_float::half>
  , param<expect<23, 34, 4, 48>, half_float::half>

  , param<expect<4207, 118, 1, 2148>, double>
  , param<expect<2104, 118, 2, 2148>, double>
  , param<expect<1052, 118, 4, 2148>, double>
  , param<expect<526,  118, 8, 2148>, double>
>;
TYPED_TEST_SUITE(ExpectTest, ExpectTypes);

TYPED_TEST(ExpectTest, SimpleAccumulations) {
  E e;
  E::line_t l(2);
  sintw<16> w(0xBEEF);

  e.accumulate(w, l);
  e.accumulate(w, l);

  EXPECT_EQ(
    e.get_line(l),
    (w * sintw<3>(2)).widen<TypeParam::expect::word_t::width>()
  );
}

TEST(ExpectTest, PositivePropagation) {
  typedef expect<278, 60, 2, 298> expect_t;
  expect_t e;
  expect_t::line_t l;

  l = 0;
  e.accumulate(uintw<8>(0xFF), l);
  l = 1;
  e.accumulate(uintw<8>(0xFF), l);
  l = 2;
  e.accumulate(uintw<8>(0xFF), l);
  l = 3;
  e.accumulate(uintw<8>(0xFF), l);
  l = 4;
  e.accumulate(uintw<8>(0xFF), l);

  e.propagate();

  l = 0;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(3)
  );
  l = 1;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(2)
  );
  l = 2;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(2)
  );
  l = 3;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(2)
  );
  l = 4;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(3)
  );
  l = 6;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(1)
  );
  l = 7;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(1)
  );
  l = 8;
  EXPECT_EQ(
    e.get_line(l),
    expect_t::word_t(1)
  );
}

TEST(ExpectTest, NegativePropagation) {
  typedef expect<278, 60, 2, 298> expect_t;
  expect_t e;
  expect_t::line_t l;

  l = 0;
  e.accumulate(sintw<8>(-19), l);
  l = 1;
  e.accumulate(sintw<8>(31), l);
  l = 2;
  e.accumulate(sintw<8>(-29), l);

  e.propagate();

  // Note: hardcoded for R=2
  l = 0;
  EXPECT_EQ(
    e.get_line(l),
    1
  );
  l = 1;
  EXPECT_EQ(
    e.get_line(l),
    2
  );
  l = 2;
  EXPECT_EQ(
    e.get_line(l),
    1
  );
  l = 3;
  EXPECT_EQ(
    e.get_line(l),
    2
  );
  l = 4;
  EXPECT_EQ(
    e.get_line(l),
    2
  );
  l = 5;
  EXPECT_EQ(
    e.get_line(l),
    -1
  );
  l = 6;
  EXPECT_EQ(
    e.get_line(l),
    0
  );
}

TEST(ExpectTest, ComplexPropagation) {
  // TODO: example in thesis is NOT up to date.
  // Example in thesis (figure fig:carrypropagationexample)
  // Note that template's first argument is B while in thesis it's W=B*R
  // We use W=24 instead of 8 to make propagate() compile (W must be > floating-point's mantissa)
  expect<6, 8, 2, 0> e;

  e.set_line(0, -19);
  e.set_line(1, 31);   // = 124
  e.set_line(2, -29);  // = -464

  std::cout << e;

  e.propagate<half_float::half>();

  EXPECT_EQ(
    e.get_line(0),
    1
  );
  EXPECT_EQ(
    e.get_line(1),
    2
  );
  EXPECT_EQ(
    e.get_line(2),
    1
  );
  EXPECT_EQ(
    e.get_line(3),
    2
  );
  EXPECT_EQ(
    e.get_line(4),
    2
  );
  EXPECT_EQ(
    e.get_line(5),
    -1
  );

  std::cout << e;
}

TYPED_TEST(ExpectTest, SimpleFloatAccumulation) {
  E e;

  e.accumulate(floatw<F>(std::numeric_limits<F>::denorm_min())).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmin()),
    1
  );
  e.reset();

  e.accumulate(floatw<F>(std::numeric_limits<F>::min())).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmin() + floatw<F>::wf()),
    1
  );
  e.reset();

  e.accumulate(FW(1.)).P();
  EXPECT_EQ(
    e.get_bit(0),
    1
  );
  e.reset();

  e.accumulate(floatw<F>(std::numeric_limits<F>::max())).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmax()),
    1
  );
  e.reset();
}

TYPED_TEST(ExpectTest, OverlapedNegativeFloatAccumulation) {
  E e;

  e.accumulate(FW(1.));
  e.accumulate(FW(-2.));
  e.P();
  e.reset();

  e.accumulate(FW(1.)).P();
  e.accumulate(FW(-2.));
  e.P();
  e.reset();
}

TYPED_TEST(ExpectTest, NarrowerFloatAccumulation) {
  E e;

  e.accumulate(floatw<half_float::half>(std::numeric_limits<half_float::half>::denorm_min())).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmin()),
    1
  );
  e.reset();

  e.accumulate(floatw<half_float::half>(std::numeric_limits<half_float::half>::min())).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmin() + floatw<half_float::half>::wf()),
    1
  );
  e.reset();

  e.accumulate(floatw<half_float::half>(half_float::half(1.))).P();
  EXPECT_EQ(
    e.get_bit(0),
    1
  );
  e.reset();

  e.accumulate(floatw<half_float::half>(std::numeric_limits<half_float::half>::max())).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmax()),
    1
  );
  e.reset();
}

TEST(ExpectTest, BiggerFloatAccumulation) {
  // Mostly here to check that the type checking passes

  // We want to accumulate a double(1.) and normalise to half
  // Constraints (assuming R=1)
  // - word will be accumulated at weight -52 => Zero ≥ 52
  // - word will be accumulated at position 53 in the line ⇒ W > 53
  // - after propagation, bit will be set at weight Zero. Need 1 more line to
  //   get the bit in the "implicit bit detection" position ⇒ B > 52+1
  expect<54, 54, 1, 52> e;

  e.accumulate(floatw<double>(1.));
  EXPECT_EQ(
    e.template propagate<half_float::half>(),
    floatw<half_float::half>(half_float::half(1.))
  );
}

TYPED_TEST(ExpectTest, SimpleFloatMAC) {
  E e;

  e.mac(
    floatw<F>(std::numeric_limits<F>::denorm_min()),
    floatw<F>(std::numeric_limits<F>::denorm_min())
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmin() * 2),
    1
  );
  e.reset();

  e.mac(
    floatw<F>(std::numeric_limits<F>::denorm_min()),
    FW(1.)
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmin()),
    1
  );
  e.reset();

  e.mac(
    FW(1.),
    FW(1.)
  ).P();
  EXPECT_EQ(
    e.get_bit(0),
    1
  );
  e.reset();

  e.mac(
    floatw<F>(std::numeric_limits<F>::max()),
    floatw<F>(std::numeric_limits<F>::max())
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<F>::wmax() * 2),
    1
  );
  e.reset();
}

TYPED_TEST(ExpectTest, NarrowerFloatMAC) {
  E e;

  e.mac(
    floatw<half_float::half>(std::numeric_limits<half_float::half>::denorm_min()),
    floatw<half_float::half>(std::numeric_limits<half_float::half>::denorm_min())
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmin() * 2),
    1
  );
  e.reset();

  e.mac(
    floatw<half_float::half>(std::numeric_limits<half_float::half>::denorm_min()),
    floatw<half_float::half>(half_float::half(1.))
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmin()),
    1
  );
  e.reset();

  e.mac(
    floatw<half_float::half>(half_float::half(1.)),
    floatw<half_float::half>(half_float::half(1.))
  ).P();
  EXPECT_EQ(
    e.get_bit(0),
    1
  );
  e.reset();

  e.mac(
    floatw<half_float::half>(std::numeric_limits<half_float::half>::max()),
    floatw<half_float::half>(std::numeric_limits<half_float::half>::max())
  ).P();
  EXPECT_EQ(
    e.get_bit(floatw<half_float::half>::wmax() * 2),
    1
  );
  e.reset();
}

TEST(ExpectTest, BiggerFloatMAC) {
  // Mostly here to check that the type checking passes

  // We want to accumulate a double product (2×3.) and normalise to half
  // Same constraints than BiggerFloatAccumulation, with a product
  expect<106, 108, 1, 102> e;

  e.mac(floatw<double>(2.), floatw<double>(3.));
  EXPECT_EQ(
    e.template propagate<half_float::half>(),
    floatw<half_float::half>(half_float::half(6.))
  );
}

TYPED_TEST(ExpectTest, NarrowerNormalisation) {
  // Mostly here to check that the type checking passes
  E e;

  e.accumulate(FW(1.));

  EXPECT_EQ(
    e.template propagate<half_float::half>(),
    floatw<half_float::half>(half_float::half(1.))
  );
}

TEST(ExpectTest, BiggerNormalisation) {
  // Mostly here to check that the type checking passes

  // We want to accumulate a half(1.) and normalise to double
  // Constraints (assuming R=1)
  // - word will be accumulated at weight -10 => Zero ≥ 10
  // - word will be accumulated at position 11 in the line ⇒ W > 11
  // - after propagation, bit will be set at weight Zero. Need 1 more line to
  //   get the bit in the "implicit bit detection" position ⇒ B > 10+1
  expect<12, 12, 1, 10> e;

  e.accumulate(floatw<half_float::half>(half_float::half(1.)));

  EXPECT_EQ(
    e.template propagate<double>(),
    floatw<double>(1.)
  );
}

TYPED_TEST(ExpectTest, SimplePositiveNormalisation) {
  E e;
  floatw<F> f;

  // smallest product
  f = std::numeric_limits<F>::denorm_min();
  EXPECT_EQ(
    e.mac(f, f).P(),
    floatw<F>(0.)
  );
  e.reset();

  // biggest non-representable value under zero
  EXPECT_EQ(
    e.mac(f, FW(0.5)).P(),
    floatw<F>(0.)
  );
  e.reset();

  // smallest representable value
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // biggest subnormal value
  f = floatw<F>(floatw<F>(std::numeric_limits<F>::min()).mem().modularPred());
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // smallest normal
  f = std::numeric_limits<F>::min();
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // a normal (1.5 + 1ULP)
  f = floatw<F>(FW(1.5f).mem().modularSucc());
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // another normal (same as before with exponent+1)
  // test exponent/mantissa alignment when R>1 (with the previous one, we are
  // sure to not get alignment by chance)
  f = floatw<F>(FW(3.).mem().modularSucc());
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // biggest representable value
  f = floatw<F>(std::numeric_limits<F>::max());
  EXPECT_EQ(
    e.accumulate(f).P(),
    f
  );
  e.reset();

  // smallest non-representable value above biggest representable value
  EXPECT_EQ(
    e.mac(f, FW(2.)).P(),
    floatw<F>::infinity()
  );
  e.reset();

  // biggest product
  EXPECT_EQ(
    e.mac(f, f).P(),
    floatw<F>::infinity()
  );
}

TYPED_TEST(ExpectTest, SimpleNegativeNormalisation) {
  E e;
  floatw<F> f;

  // Same cases as SimplePositiveNormalisation with negative result

  // smallest product
  f = std::numeric_limits<F>::denorm_min();
  EXPECT_EQ(
    e.mac(f, -f).P(),
    -floatw<F>(0.)
  );
  e.reset();

  // biggest non-representable value under zero
  EXPECT_EQ(
    e.mac(f, FW(-0.5f)).P(),
    -floatw<F>(0.)
  );
  e.reset();

  // smallest representable value
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // biggest subnormal value
  f = floatw<F>(floatw<F>(std::numeric_limits<F>::min()).mem().modularPred());
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // smallest normal
  f = std::numeric_limits<F>::min();
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // a normal (1.5 + 1ULP)
  f = floatw<F>(FW(1.5f).mem().modularSucc());
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // another normal (same as before with exponent+1)
  // test exponent/mantissa alignment when R>1 (with the previous one, we are
  // sure to not get alignment by chance)
  f = floatw<F>(FW(3.).mem().modularSucc());
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // biggest representable value
  f = floatw<F>(std::numeric_limits<F>::max());
  EXPECT_EQ(
    e.accumulate(-f).P(),
    -f
  );
  e.reset();

  // smallest non-representable value above biggest representable value
  EXPECT_EQ(
    e.mac(f, FW(-2.)).P(),
    -floatw<F>::infinity()
  );
  e.reset();

  // biggest product
  EXPECT_EQ(
    e.mac(f, -f).P(),
    -floatw<F>::infinity()
  );
}

TYPED_TEST(ExpectTest, NonSignificantPositiveNormalisation) {
  // Test a normalisation with some non significant bits (below mantissa's LSB)
  E e;
  floatw<F> f;

  // a normal (1.5 + 1ULP)
  f = floatw<F>(FW(1.5f).mem().modularSucc());

  e.accumulate(f);
  e.accumulate(FW(pow(2., -floatw<F>::wf() - 2)));
  EXPECT_EQ(
    e.P(),
    f
  );
  e.reset();

  // Same with negative non significant bits
  // The -1 propagated until the significand remove the +1ULP
  e.accumulate(f);
  e.accumulate(-FW(pow(2., -floatw<F>::wf() - 4)));
  EXPECT_EQ(
    e.P(),
    FW(1.5f)
  );
  e.reset();
}

TYPED_TEST(ExpectTest, NonSignificantNegativeNormalisation) {
  // Test a normalisation with some non significant bits (below mantissa's LSB)
  E e;
  floatw<F> f;

  // a normal (1.5 + 1ULP)
  f = floatw<F>(FW(1.5f).mem().modularSucc());

  e.accumulate(-f);
  e.accumulate(-FW(pow(2., -floatw<F>::wf() - 2)));
  EXPECT_EQ(
    e.P(),
    -f
  );
  e.reset();

  // Same with positive non significant bits
  // The +1 propagated until the significand remove the +1ULP
  e.accumulate(-f);
  e.accumulate(FW(pow(2., -floatw<F>::wf() - 2)));
  EXPECT_EQ(
    e.P(),
    -FW(1.5f)
  );
  e.reset();
}

TYPED_TEST(ExpectTest, DoubleNegativeNormalisation) {
  // Test normalisation of 2 "subsequent" negative numbers

  E e;
  floatw<F> f1, f2;

  f1 = FW(-31.);
  f2 = f1 * FW(pow(2., floatw<F>::wf() + 6));

  e.accumulate(f1);
  e.accumulate(f2);

  EXPECT_EQ(
    e.P(),
    f2
  );
}

TEST(ExpectTest, ComplexNormalisation) {
  // Exemple from thesis figure fig:carrypropagationexample
  // (with larger E format)
  expect<278, 60, 2, 298> e;
  e.set_line(149, -19);
  e.set_line(150, 31);   // = 124
  e.set_line(151, -29); // = -464

  std::cout << e;

  EXPECT_EQ(
    e.propagate(),
    floatw<float>(-359.f)
  );

  std::cout << e;

  EXPECT_EQ(
    e.propagate(),
    floatw<float>(-359.f)
  );

  std::cout << e;
}

TYPED_TEST(ExpectTest, CaptureResolutionNormalisation) {
  E e;

  // Represent -1 as -32+16+8+4+2+1
  e.accumulate(FW(-32.)).P();
  e.accumulate(FW(16.)).P();
  e.accumulate(FW(8.)).P();
  e.accumulate(FW(4.)).P();
  e.accumulate(FW(2.)).P();
  e.accumulate(FW(1.)).P();

  EXPECT_EQ(
    e.P(),
    FW(-1.)
  );
  e.reset();

  // Represent -0.x as -32+16+8+4+2+1+0.x
  // -0.x is the number directly below -1
  // test that a non-zero mantissa is handled correctly in this case
  e.accumulate(FW(-32.)).P();
  e.accumulate(FW(16.)).P();
  e.accumulate(FW(8.)).P();
  e.accumulate(FW(4.)).P();
  e.accumulate(FW(2.)).P();
  e.accumulate(FW(1.)).P();
  e.accumulate(FW(pow(2., -floatw<F>::wf() - 1))).P();

  // Beware: this number is written as -1[1 7e 7fffff] (for basefloat=float)
  // because of rounding but its value is actually 0.99… Its exponent is 0x7e
  // (-1), not 7f (0)!
  EXPECT_EQ(
    e.P(),
    floatw<F>(FW(-1.).mem().modularPred())
  );
  e.reset();

  // Represent -1.x as -32+16+8+4+2+1-0.x
  // -1.x is the number directly above -1
  // test that this case still works when we are currently dealing with a
  // negative value
  e.accumulate(FW(-32.)).P();
  e.accumulate(FW(16.)).P();
  e.accumulate(FW(8.)).P();
  e.accumulate(FW(4.)).P();
  e.accumulate(FW(2.)).P();
  e.accumulate(FW(1.)).P();
  e.accumulate(-FW(pow(2., -floatw<F>::wf()))).P();

  EXPECT_EQ(
    e.P(),
    floatw<F>(FW(-1.).mem().modularSucc())
  );
  e.reset();

  // Test with a "bubble" (0) in the scattered "sign bits" to verify that
  // switching off and then directly on this "mode" works well.
  e.accumulate(FW(-128.)).P();
  e.accumulate(FW(64.)).P();
  e.accumulate(FW(32.)).P();
  e.accumulate(FW(16.)).P();
  // 8: bubble
  e.accumulate(FW(4.)).P();
  e.accumulate(FW(2.)).P();
  e.accumulate(FW(1.)).P();

  EXPECT_EQ(
    e.P(),
    FW(-1. - 8.)
  );
  e.reset();

  // Represent -1 as -32+31 (31 in one line, at line 2^0)
  e.accumulate(FW(-32.)).P();
  // accumulate 31 + x such that 31 is written where it would be propagated,
  // but in one line
  e.accumulate(FW(31. + pow(2., floatw<F>::wf())));
  // remove extra x
  e.accumulate(FW(-pow(2., floatw<F>::wf())));

  EXPECT_EQ(
    e.P(),
    FW(F(-1.))
  );
  e.reset();

  // Control test: an accumulator state that will trigger this mechanism but
  // the normalisation result do not depend on it
  e.accumulate(FW(16.)).P();  // a set of scattered
  e.accumulate(FW(8.)).P();   // false "sign bits"
  e.accumulate(FW(4.)).P();
  e.accumulate(FW(2.)).P();
  e.accumulate(FW(1.)).P();
  e.accumulate(FW(-64.)).P(); // trigger State::neg

  EXPECT_EQ(
    e.P(),
    FW(31. - 64.)
  );
}

TYPED_TEST(ExpectTest, NaN) {
  E e;
  floatw<F> one(FW(1.));
  floatw<F> zero(FW(0.));
  floatw<F> inf(floatw<F>::infinity());
  floatw<F> ninf(-floatw<F>::infinity());
  floatw<F> quiet_NaN(floatw<F>::quiet_NaN());
  floatw<F> signaling_NaN(floatw<F>::signaling_NaN());

  e.accumulate(quiet_NaN);
  EXPECT_TRUE(e.P().isnan());
  e.reset();

  e.accumulate(signaling_NaN);
  EXPECT_TRUE(e.P().isnan());
  e.reset();

  e.accumulate(one);
  e.accumulate(quiet_NaN);
  EXPECT_TRUE(e.P().isnan());
  e.reset();

  e.mac(one, quiet_NaN);
  EXPECT_TRUE(e.P().isnan());
  e.reset();

  e.mac(inf, zero);
  EXPECT_TRUE(e.P().isnan());
  e.reset();

  e.accumulate(ninf);
  e.accumulate(-ninf);
  EXPECT_TRUE(e.P().isnan());
}

TYPED_TEST(ExpectTest, Infinity) {
  E e;
  floatw<F> inf(floatw<F>::infinity());
  floatw<F> ninf(-floatw<F>::infinity());

  EXPECT_TRUE(inf.isinf());
  EXPECT_TRUE(ninf.isinf());

  e.accumulate(inf);
  EXPECT_EQ(
    e.P(),
    inf
  );
  e.reset();

  e.accumulate(inf);
  e.accumulate(inf);
  EXPECT_EQ(
    e.P(),
    inf
  );
  e.reset();

  e.accumulate(ninf);
  EXPECT_EQ(
    e.P(),
    ninf
  );
  e.reset();

  e.accumulate(ninf);
  e.accumulate(ninf);
  EXPECT_EQ(
    e.P(),
    ninf
  );
  e.reset();

  e.mac(inf, inf);
  EXPECT_EQ(
    e.P(),
    inf
  );
  e.reset();

  e.mac(ninf, ninf);
  EXPECT_EQ(
    e.P(),
    inf
  );
  e.reset();

  e.mac(inf, ninf);
  EXPECT_EQ(
    e.P(),
    ninf
  );
  e.reset();
}

TYPED_TEST(ExpectTest, SimpleEqualComparisons) {
  E l, r;

  l.accumulate(FW(1.));
  r.accumulate(FW(1.));

  EXPECT_TRUE(l == r);
  EXPECT_TRUE(r == l);

  r.accumulate(FW(1.));

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
  l.reset();
  r.reset();

  // Set most significant bit (B×R)
  l.accumulate(
    E::word_t(1 << (TypeParam::expect::R - 1)),
    E::line_t(TypeParam::expect::B - 1)
  );

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);

  r.accumulate(
    E::word_t(1 << (TypeParam::expect::R - 1)),
    E::line_t(TypeParam::expect::B - 1)
  );

  EXPECT_TRUE(l == r);
  EXPECT_TRUE(r == l);
}

TYPED_TEST(ExpectTest, SimpleEqualPropagatedComparisons) {
  E l, r;

  l.accumulate(FW(1.));
  r.accumulate(FW(1.)).P(); // propagate only 1 of them

  EXPECT_TRUE(l == r);
  EXPECT_TRUE(r == l);

  r.accumulate(FW(1.));

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
}

TYPED_TEST(ExpectTest, NaNEqualComparisons) {
  E l, r;

  l.accumulate(FW(1.));
  r.accumulate(floatw<F>::quiet_NaN());

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
  l.reset();
  r.reset();

  l.accumulate(floatw<F>::infinity());
  r.accumulate(floatw<F>::quiet_NaN());

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
  l.reset();
  r.reset();

  l.accumulate(floatw<F>::quiet_NaN());
  r.accumulate(floatw<F>::quiet_NaN());

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
}

TYPED_TEST(ExpectTest, InfinityEqualComparisons) {
  E l, r;

  floatw<F> inf(floatw<F>::infinity());
  floatw<F> ninf(-floatw<F>::infinity());

  l.accumulate(inf);
  r.accumulate(inf);

  EXPECT_TRUE(l == r);
  EXPECT_TRUE(r == l);
  l.reset();
  r.reset();

  l.accumulate(ninf);
  r.accumulate(ninf);

  EXPECT_TRUE(l == r);
  EXPECT_TRUE(r == l);
  l.reset();
  r.reset();

  l.accumulate(inf);
  r.accumulate(ninf);

  EXPECT_FALSE(l == r);
  EXPECT_FALSE(r == l);
}

TYPED_TEST(ExpectTest, FuzzyEqualComparisons) {
  // TODO: support W>sizeof(long int)
  // Compare a random expect with the same, but propagated

  std::random_device rd;
  std::mt19937 gen(42);

  // number of accumulation
  std::exponential_distribution<double> dist_a(0.1);
  // accumulated value width: W (FIXME: saturate at 64 bits because we draw a
  // unsigned int)
  std::size_t w = std::min(
    8 * sizeof(long int),
    std::size_t(TypeParam::expect::W)
  );
  // accumulated value "mantissa"
  std::uniform_int_distribution<long int> dist_value(
    -(1L << (w - 1)),
    (1L << (w - 1)) - 1
  );
  std::uniform_int_distribution<long int> dist_line(
    0,
    TypeParam::expect::B - 1
  );

  for(int fuzzy = 0; fuzzy < 1000; fuzzy++) {
    E l, r;

    // Draw number of accumulations
    int accs = 1 + dist_a(gen);
    for(int acc = 0; acc < accs; acc++) {
      // draw value and line
      long int value = dist_value(gen);
      long int line = dist_line(gen);

      l.accumulate(
        E::word_t(value),
        E::line_t(line)
      );
      r.accumulate(
        E::word_t(value),
        E::line_t(line)
      );
    }

    EXPECT_TRUE(l == r);
    EXPECT_TRUE(r == l);

    l.propagate();

    EXPECT_TRUE(l == r);
    EXPECT_TRUE(r == l);

    long int line = dist_line(gen);
    l.accumulate(
      E::word_t(1),
      E::line_t(line)
    );

    EXPECT_FALSE(l == r);
    EXPECT_FALSE(r == l);
  }
}

TYPED_TEST(ExpectTest, FuzzyExpectAccumulations) {
  // TODO: support W>sizeof(long int)
  // Compare a random expect with the same, but propagated

  std::random_device rd;
  std::mt19937 gen(42);

  // number of accumulation
  std::exponential_distribution<double> dist_a(0.1);
  // accumulated value width: W (FIXME: saturate at 64 bits because we draw a
  // unsigned int)
  std::size_t w = std::min(
    8 * sizeof(long int),
    std::size_t(TypeParam::expect::W)
  );
  // accumulated value "mantissa"
  std::uniform_int_distribution<long int> dist_value(
    -(1L << (w - 1)),
    (1L << (w - 1)) - 1
  );
  std::uniform_int_distribution<long int> dist_line(
    0,
    TypeParam::expect::B - 1
  );

  for(int fuzzy = 0; fuzzy < 1000; fuzzy++) {
    E l, r, total;

    // Draw number of accumulations
    int accs = 1 + dist_a(gen);
    for(int acc = 0; acc < accs; acc++) {
      // draw value and line
      long int value = dist_value(gen);
      long int line = dist_line(gen);

      l.accumulate(
        E::word_t(value),
        E::line_t(line)
      );

      total.accumulate(
        E::word_t(value),
        E::line_t(line)
      );
    }

    // Same for r
    accs = 1 + dist_a(gen);
    for(int acc = 0; acc < accs; acc++) {
      // draw value and line
      long int value = dist_value(gen);
      long int line = dist_line(gen);

      r.accumulate(
        E::word_t(value),
        E::line_t(line)
      );

      total.accumulate(
        E::word_t(value),
        E::line_t(line)
      );
    }

    l.accumulate(r);

    EXPECT_TRUE(l == total);
  }
}

TYPED_TEST(ExpectTest, FuzzyNormalisation) {
  // TODO: do not really test subnormals, change the random distributions
  // TODO: support W>sizeof(long int)

  std::random_device rd;
  std::mt19937 gen(42);

  // number of accumulation (saturated at 1024 below)
  std::exponential_distribution<double> dist_a(0.1);
  // accumulated value width: to support up to 1024 accumulation we remove 10-1+R
  // bits. (FIXME: saturate at 64-(R-1) bits because we draw a unsigned int (-R
  // because the word can be shifted left by R-1 position))
  std::size_t w = std::min(
    8 * sizeof(long int) - (TypeParam::expect::R - 1),
    std::size_t(TypeParam::expect::word_t::width) - 9 - TypeParam::expect::R
  );
  // accumulated value "mantissa"
  std::uniform_int_distribution<long int> dist_value(
    -(1L << (w - 1)),
    (1L << (w - 1)) - 1
  );
  // accumulated value weight: binomial distribution centered around zero
  // Note: the highest weight is computed such that we can safely accumulate up
  // to 1024 float products without overflowing.
  std::binomial_distribution<long int> dist_weight(
    TypeParam::expect::B * TypeParam::expect::R - 1 // maximum weight
      - 10  // support up to 1024 accumulations of…
      - 2 * (floatw<F>::wf() + 1), // float products
    double(TypeParam::expect::Zero) / double(TypeParam::expect::B * TypeParam::expect::R)
  );

  mpfr_t m_value, m_weight, m_acc;
  // Set MPFR's significant to have enough bits to store both accumlator's MSB
  // and LSB
  mpfr_set_default_prec(TypeParam::expect::B * TypeParam::expect::R);
  mpfr_inits(m_value, m_weight, m_acc, NULL);

  for(int fuzzy = 0; fuzzy < 1000; fuzzy++) {
    E e;
    mpfr_set_zero(m_acc, 1);

    // Draw number of accumulations
    // Saturate at 1024 because otherwise a line could overflow (because we
    // accumulate word of length W-(10+1)R
    int accs = std::min(1 + dist_a(gen), 1024.);
    for(int acc = 0; acc < accs; acc++) {
      // draw value and weight
      long int value = dist_value(gen);
      long int weight = dist_weight(gen);

      // convert value and weight to MPFR variables
      mpfr_set_si(m_value, value, MPFR_RNDZ);
      mpfr_set_si(m_weight, weight - TypeParam::expect::Zero, MPFR_RNDZ);

      // compute 2^m_weight
      mpfr_exp2(m_weight, m_weight, MPFR_RNDZ);
      // compute m_value * 2^m_weight
      mpfr_mul(m_value, m_value, m_weight, MPFR_RNDZ);

      // perform accumulation
      mpfr_add(m_acc, m_acc, m_value, MPFR_RNDZ);
      e.accumulate(
        E::word_t(value << (weight % TypeParam::expect::R)),
        E::line_t(weight / TypeParam::expect::R)
      );
    }

    // convert to different float formats
    floatw<float> resf(e.template propagate<float>());
    floatw<float> reff(mpfr_get_flt(m_acc, MPFR_RNDZ));

    floatw<double> resd(e.template propagate<double>());
    floatw<double> refd(mpfr_get_d(m_acc, MPFR_RNDZ));

    /* std::cout << resf << " vs " << reff << '\n'; */
    /* std::cout << resd << " vs " << refd << '\n'; */

    // Check that the value did not overflowed
    // Note: with rounding away from zero, MPFR returns ±inf while with
    // rounding toward zero it returns ±max (but returns the same value as the
    // accumulator otherwise)
    if(std::isinf(mpfr_get_flt(m_acc, MPFR_RNDA))) {
      EXPECT_TRUE(resf.isinf());
      EXPECT_TRUE(!mpfr_signbit(m_acc) || resf.sign()); // mpfr_signbit(m_acc) -> resf.sign()
      EXPECT_TRUE(mpfr_signbit(m_acc) || !resf.sign()); // !mpfr_signbit(m_acc) -> !resf.sign()
    }
    else
      EXPECT_EQ(resf, reff);

    if(std::isinf(mpfr_get_d(m_acc, MPFR_RNDA))) {
      EXPECT_TRUE(resd.isinf());
      EXPECT_TRUE(!mpfr_signbit(m_acc) || resd.sign()); // mpfr_signbit(m_acc) -> resd.sign()
      EXPECT_TRUE(mpfr_signbit(m_acc) || !resd.sign()); // !mpfr_signbit(m_acc) -> !resd.sign()
    }
    else
      EXPECT_EQ(resd, refd);
  }

  mpfr_clears(m_value, m_weight, m_acc, NULL);
}
