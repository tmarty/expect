#include <gtest/gtest.h>
#include <iostream>
#include <limits>
#include <half.hpp>
#include "floatwrapper.h"

template <typename T>
class FloatWrapperTest : public ::testing::Test {
};

using FloatTypes = ::testing::Types<
  half_float::half,
  float,
  double
>;
TYPED_TEST_SUITE(FloatWrapperTest, FloatTypes);

TEST(FloatWrapperTest, StaticConstants) {
  // Note: this test is not portable

  EXPECT_EQ(
    floatw<half_float::half>::we(),
    5
  );
  EXPECT_EQ(
    floatw<half_float::half>::wf(),
    10
  );
  EXPECT_EQ(
    floatw<half_float::half>::bias(),
    15
  );
  EXPECT_EQ(
    floatw<half_float::half>::wmin(),
    -24
  );
  EXPECT_EQ(
    floatw<half_float::half>::wmax(),
    15
  );

  EXPECT_EQ(
    floatw<float>::we(),
    8
  );
  EXPECT_EQ(
    floatw<float>::wf(),
    23
  );
  EXPECT_EQ(
    floatw<float>::bias(),
    127
  );
  EXPECT_EQ(
    floatw<float>::wmin(),
    -149
  );
  EXPECT_EQ(
    floatw<float>::wmax(),
    127
  );

  EXPECT_EQ(
    floatw<double>::we(),
    11
  );
  EXPECT_EQ(
    floatw<double>::wf(),
    52
  );
  EXPECT_EQ(
    floatw<double>::bias(),
    1023
  );
  EXPECT_EQ(
    floatw<double>::wmin(),
    -1074
  );
  EXPECT_EQ(
    floatw<double>::wmax(),
    1023
  );
}

TEST(FloatWrapperTest, StaticConstantsWrapper) {
  // Note: this test is not portable

  EXPECT_EQ(
    floatw<half_float::half>::wew(),
    sintw<4>(5)
  );
  EXPECT_EQ(
    floatw<half_float::half>::wfw(),
    sintw<5>(10)
  );
  EXPECT_EQ(
    floatw<half_float::half>::biasw(),
    sintw<5>(15)
  );
  EXPECT_EQ(
    floatw<half_float::half>::wminw(),
    sintw<6>(-24)
  );
  EXPECT_EQ(
    floatw<half_float::half>::wmaxw(),
    sintw<5>(15)
  );

  EXPECT_EQ(
    floatw<float>::wew(),
    sintw<5>(8)
  );
  EXPECT_EQ(
    floatw<float>::wfw(),
    sintw<6>(23)
  );
  EXPECT_EQ(
    floatw<float>::biasw(),
    sintw<8>(127)
  );
  EXPECT_EQ(
    floatw<float>::wminw(),
    sintw<9>(-149)
  );
  EXPECT_EQ(
    floatw<float>::wmaxw(),
    sintw<8>(127)
  );

  EXPECT_EQ(
    floatw<double>::wew(),
    sintw<5>(11)
  );
  EXPECT_EQ(
    floatw<double>::wfw(),
    sintw<7>(52)
  );
  EXPECT_EQ(
    floatw<double>::biasw(),
    sintw<11>(1023)
  );
  EXPECT_EQ(
    floatw<double>::wminw(),
    sintw<12>(-1074)
  );
  EXPECT_EQ(
    floatw<double>::wmaxw(),
    sintw<11>(1023)
  );
}

TYPED_TEST(FloatWrapperTest, Exponent) {
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::denorm_min()).exponent(),
    0
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::min()).exponent(),
    1
  );
  EXPECT_EQ(
    floatw<TypeParam>(TypeParam(1.)).exponent(),
    floatw<TypeParam>::bias()
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::max()).exponent(),
    floatw<TypeParam>::bias() + floatw<TypeParam>::wmax()
  );
}

TYPED_TEST(FloatWrapperTest, CorrectedExponent) {
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::denorm_min()).corrected_exponent(),
    1
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::min()).corrected_exponent(),
    1
  );
  EXPECT_EQ(
    floatw<TypeParam>(TypeParam(1.)).corrected_exponent(),
    floatw<TypeParam>::bias()
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::max()).corrected_exponent(),
    floatw<TypeParam>::bias() + floatw<TypeParam>::wmax()
  );
}

TYPED_TEST(FloatWrapperTest, RealExponent) {
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::denorm_min()).real_exponent(),
    -floatw<TypeParam>::bias() + 1
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::min()).real_exponent(),
    -floatw<TypeParam>::bias() + 1
  );
  EXPECT_EQ(
    floatw<TypeParam>(TypeParam(1.)).real_exponent(),
    0
  );
  EXPECT_EQ(
    floatw<TypeParam>(std::numeric_limits<TypeParam>::max()).real_exponent(),
    floatw<TypeParam>::wmax()
  );
}

/* TYPED_TEST(FloatWrapperTest, Sign) { */
/*   floatw<TypeParam> p(TypeParam(1.)), n(TypeParam(-1.)); */
/*   EXPECT_EQ( */
/*     p.sign(), */
/*     p.sign() */
/*   ); */
/*   /1* EXPECT_EQ( *1/ */
/*   /1*   n.sign(), *1/ */
/*   /1*   1 *1/ */
/*   /1* ); *1/ */
/* } */

TYPED_TEST(FloatWrapperTest, ZeroFields) {
  floatw<TypeParam> f(TypeParam(0.));
  /* EXPECT_EQ(f.sign(), 0); */
  /* EXPECT_EQ(f.exponent(), 0); */
  /* EXPECT_EQ(f.mantissa(), 0); */
}

/* TYPED_TEST(FloatWrapperTest, OneFields) { */
/*   TypeParam f(1.); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), float_fields<TypeParam>::exponent_bias()); */
/*   EXPECT_EQ(float_mantissa(f), 0); */
/* } */

/* TYPED_TEST(FloatWrapperTest, NegFields) { */
/*   TypeParam f(1.); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   f = -f; */
/*   EXPECT_EQ(float_sign(f), 1); */
/* } */

/* TYPED_TEST(FloatWrapperTest, MinFields) { */
/*   TypeParam f = std::numeric_limits<TypeParam>::min(); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), 1); */
/*   EXPECT_EQ(float_mantissa(f), 0); */
/* } */

/* TYPED_TEST(FloatWrapperTest, DenormMinFields) { */
/*   TypeParam f = std::numeric_limits<TypeParam>::denorm_min(); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), 0); */
/*   EXPECT_EQ(float_mantissa(f), 1); */
/* } */

/* TYPED_TEST(FloatWrapperTest, MaxFields) { */
/*   TypeParam f = std::numeric_limits<TypeParam>::max(); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), (1UL << float_fields<TypeParam>::exponent_bits()) - 2); */
/*   EXPECT_EQ(float_mantissa(f), (1UL << float_fields<TypeParam>::mantissa_bits()) - 1); */
/* } */

/* TYPED_TEST(FloatWrapperTest, InfFields) { */
/*   TypeParam f = std::numeric_limits<TypeParam>::infinity(); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), (1UL << float_fields<TypeParam>::exponent_bits()) - 1); */
/*   EXPECT_EQ(float_mantissa(f), 0); */
/* } */

/* TYPED_TEST(FloatWrapperTest, NaNFields) { */
/*   TypeParam f = std::numeric_limits<TypeParam>::quiet_NaN(); */
/*   EXPECT_EQ(float_sign(f), 0); */
/*   EXPECT_EQ(float_exponent(f), (1UL << float_fields<TypeParam>::exponent_bits()) - 1); */
/*   EXPECT_NE(float_mantissa(f), 0); */
/* } */
