#include <gtest/gtest.h>
#include <iostream>
#include "intwrapper.h"

TEST(IntWrapperTest, UnsignedSumWidenResult) {
  uintw<8> a(0xFF), b(0xFF);

  EXPECT_EQ(
    a + b,
    uintw<9>(0x1FE)
  );
}

TEST(IntWrapperTest, SignedSumWidenResult) {
  sintw<8> a(0xFF), b(0xFF);

  EXPECT_EQ(
    a + b,
    sintw<9>(0x1FE)
  );
}

TEST(IntWrapperTest, UnsignedProductWidenResult) {
  uintw<8> a(0xFF);
  uintw<3> b(0x7);

  EXPECT_EQ(
    a * b,
    uintw<11>(0x6F9)
  );
}

TEST(IntWrapperTest, SignedProductWidenResult) {
  sintw<8> o8(-1);
  sintw<3> o3(-1);
  sintw<8> m8(-128);
  sintw<3> m3(-4);
  sintw<8> M8(127);
  sintw<3> M3(3);

  EXPECT_EQ(
    o8 * o3,
    sintw<11>(1)
  );
  EXPECT_EQ(
    m8 * m3,
    sintw<11>(512)
  );
  EXPECT_EQ(
    M8 * M3,
    sintw<11>(381)
  );

  sintw<16> w(0xBEEF); // note: this number is negative on 16 bits (sign bit set), equal to -16657
  EXPECT_EQ(
    w * sintw<2>(-2),
    sintw<18>(-16657 * -2)
  );
}

TEST(IntWrapperTest, ModularAdd) {
  uintw<8> a(0xFF), b(0xFF);

  EXPECT_EQ(
    a.modularAdd(b),
    uintw<8>(0xFE)
  );
}

TEST(IntWrapperTest, ReductionNone) {
  uintw<8> x(0);

  EXPECT_EQ(
    x.and_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.or_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.xor_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.nand_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.nor_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.xnor_reduce(),
    uintw<1>(1)
  );
}

TEST(IntWrapperTest, ReductionSome) {
  uintw<8> x(0xF1);

  EXPECT_EQ(
    x.and_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.or_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.xor_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.nand_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.nor_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.xnor_reduce(),
    uintw<1>(0)
  );
}

TEST(IntWrapperTest, ReductionFull) {
  uintw<8> x(0xFF);

  EXPECT_EQ(
    x.and_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.or_reduce(),
    uintw<1>(1)
  );
  EXPECT_EQ(
    x.xor_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.nand_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.nor_reduce(),
    uintw<1>(0)
  );
  EXPECT_EQ(
    x.xnor_reduce(),
    uintw<1>(1)
  );
}

TEST(IntWrapperTest, Sign) {
  uintw<1> u(1);
  sintw<1> s(1);

  EXPECT_EQ(
    (bool) uintw<2>::signness,
    false
  );
  EXPECT_EQ(
    (bool) sintw<1>::signness,
    true
  );

  EXPECT_EQ(
    s.sign(),
    true
  );

  s = 0;

  EXPECT_EQ(
    s.sign(),
    false
  );
}

TEST(IntWrapperTest, Concat) {
  uintw<3> a(1);
  uintw<5> b(1);

  EXPECT_EQ(
    (a, b),
    uintw<8>(0x21)
  );

  EXPECT_EQ(
    b.concat(a),
    uintw<8>(0x09)
  );
}

TEST(IntWrapperTest, Mux) {
  EXPECT_EQ(
    (uintw<8>::mux(false, 42, 24)),
    24
  );
  EXPECT_EQ(
    (uintw<8>::mux(true, 42, 24)),
    42
  );
}

TEST(IntWrapperTest, UnsignedShiftRight) {
  uintw<8> a(0xFF);
  uintw<8>::shift_t s(1);
  EXPECT_EQ(
    a >> s,
    uintw<8>(0x7F)
  );

  s = 8;
  EXPECT_EQ(
    a >> s,
    uintw<8>(0)
  );

  EXPECT_EQ(
    a >> uintw<1>(1),
    uintw<8>(0x7F)
  );
}

TEST(IntWrapperTest, SignedShiftRight) {
  sintw<8> a(0xFF);
  sintw<8>::shift_t s(1);
  EXPECT_EQ(
    a >> s,
    sintw<8>(0xFF)
  );

  s = 8;
  EXPECT_EQ(
    a >> s,
    sintw<8>(0xFF)
  );

  EXPECT_EQ(
    a >> uintw<1>(1),
    sintw<8>(0xFF)
  );

  a = 0x7F;
  s = 1;
  EXPECT_EQ(
    a >> s,
    sintw<8>(0x3F)
  );

  s = 8;
  EXPECT_EQ(
    a >> s,
    sintw<8>(0)
  );

  EXPECT_EQ(
    a >> uintw<1>(1),
    sintw<8>(0x3F)
  );
}

TEST(IntWrapperTest, ShiftLeft) {
  uintw<8> a(0xFF);
  EXPECT_EQ(
    a << uintw<1>(1),
    uintw<9>(0x1FE)
  );
  EXPECT_EQ(
    a << uintw<2>(3),
    uintw<11>(0x7F8)
  );
  EXPECT_EQ(
    ((a << uintw<7>(127)).slice<134, 123>()),
    uintw<12>(0xFF0)
  );
}

TEST(IntWrapperTest, ShiftLeftByZeroW) {
  uintw<16> a(0xFF);

  EXPECT_EQ(
    a << uintw<0>(0),
    a
  );
}

TEST(IntWrapperTest, SignedShiftRightByZeroW) {
  sintw<16> a(0xFF);

  EXPECT_EQ(
    a >> uintw<0>(0),
    a
  );

  a = 0x7F;

  EXPECT_EQ(
    a >> uintw<0>(0),
    a
  );
}

TEST(IntWrapperTest, UnsignedShiftRightByZeroW) {
  uintw<16> a(0xFF);

  EXPECT_EQ(
    a >> uintw<0>(0),
    a
  );
}

TEST(IntWrapperTest, RightInsert) {
  uintw<16> a(0xdead); // 1101111010101101
  uintw<3> b(0x5);     //                  101

  EXPECT_EQ(
    a.rinsert(b),
    uintw<16>(0xf56d)  // ___1111010101101 101
  );
}

TEST(IntWrapperTest, LeftInsert) {
  uintw<16> a(0xdead); //     1101111010101101
  uintw<3> b(0x5);     // 101

  EXPECT_EQ(
    a.linsert(b),
    uintw<16>(0xbbd5)  // 101 1101111010101___
  );
}

TEST(IntWrapperTest, LOP) {
  uintw<8> a;
  uintw<7> b;

  a = 0x1;
  EXPECT_EQ(
    a.lop(),
    0
  );

  a = 0x2;
  EXPECT_EQ(
    a.lop(),
    1
  );

  a = 0x7;
  EXPECT_EQ(
    a.lop(),
    2
  );

  a = 0xaa;
  EXPECT_EQ(
    a.lop(),
    7
  );

  a = 0x0;
  EXPECT_EQ(
    a.lop(),
    -1
  );

  b = 0x1;
  EXPECT_EQ(
    b.lop(),
    0
  );

  b = 0x7f;
  EXPECT_EQ(
    b.lop(),
    6
  );

  b = 0x0;
  EXPECT_EQ(
    b.lop(),
    -1
  );
}

TEST(IntWrapperTest, UnsignedSplit) {
  uintw<16> a(0xdead);
  uintw<7> s0;
  uintw<9> s1;

  a.split(s0, s1);
  EXPECT_EQ(
    s0,
    uintw<7>(0x6f)
  );
  EXPECT_EQ(
    s1,
    uintw<9>(0xad)
  );

  a.split(s1, s0);
  EXPECT_EQ(
    s1,
    uintw<9>(0x1bd)
  );
  EXPECT_EQ(
    s0,
    uintw<7>(0x2d)
  );
}

TEST(IntWrapperTest, SignedSplit) {
  sintw<16> a(0xdead);
  uintw<7> s0; // mixing signed and unsigned
  sintw<9> s1;

  a.split(s0, s1);
  EXPECT_EQ(
    s0,
    uintw<7>(0x6f)
  );
  EXPECT_EQ(
    s1,
    sintw<9>(0xad)
  );

  a.split(s1, s0);
  EXPECT_EQ(
    s1,
    sintw<9>(0x1bd)
  );
  EXPECT_EQ(
    s0,
    uintw<7>(0x2d)
  );
}

TEST(IntWrapperTest, SplitZeroW) {
  uintw<16> a(0xdead);
  uintw<0> s0;
  uintw<16> s1;

  a.split(s0, s1);
  EXPECT_EQ(
    s1,
    a
  );

  a.split(s1, s0);
  EXPECT_EQ(
    s1,
    a
  );
}

TEST(IntWrapperTest, UnsignedWiden) {
  uintw<8> u(0xFF);
  EXPECT_EQ(
    u.widen<9>(),
    uintw<9>(0xFF)
  );
}

TEST(IntWrapperTest, SignedWiden) {
  sintw<8> s(0xFF);
  EXPECT_EQ(
    s.widen<9>(),
    sintw<9>(0x1FF)
  );
}

TEST(IntWrapperTest, ConvertToIntw) {
  char c(0xAA);

  EXPECT_EQ(
    uintw<8>::convert(c).template get<7>(),
    uintw<1>(1)
  );

  float f(1);

  EXPECT_EQ(
    (sintw<32>::convert(f).template slice<30, 23>()),
    uintw<8>(127)
  );
}

TEST(IntWrapperTest, ConvertFromIntw) {
  uintw<8> c(0xAA);

  EXPECT_EQ(
    c.convert<char>(),
    char(0xAA)
  );

  sintw<32> f(0x3F800000);

  EXPECT_EQ(
    f.convert<float>(),
    float(1.)
  );
}

TEST(IntWrapperTest, ToBinString) {
  uintw<32> v(0xDEADBEEF);
  EXPECT_EQ(
    v.toBinString(),
    std::string("11011110101011011011111011101111")
  );
}
